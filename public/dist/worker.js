
// worker 的反馈动作
const actions = {
    f_init: { name: "finit" }
}

// 动作处理过程
const action_handler = (action) => {
    switch (action.name){
        case "init": {
            return self.postMessage(actions.f_init);
        }
        default: {
            console.log("worker 内 无匹配 ...");
        }
    }
};

// ...
{
    self.onmessage = ({ data }) => {
        action_handler(data);
    };
}

