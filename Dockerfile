FROM node:10.9.0

EXPOSE 3001

ENV AP /main

WORKDIR $AP

ADD ./package.json $AP

RUN npm install --production --registry=https://registry.npm.taobao.org
RUN npm install babel-cli -g --registry=https://registry.npm.taobao.org

ADD ./.env.docker $AP/.env

ADD . $AP

CMD [ "babel-node", "index.js" ]


