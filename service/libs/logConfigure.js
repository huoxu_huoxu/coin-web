/**
 * @description
 *  日志体系
 * 
 * @function
 *  logger              接口请求/返回 日志记录
 *  loginInfo           用户登录信息
 * 
 */

const log4js = require("log4js");

log4js.configure({
    appenders: {
        everything: { type: "stdout", pattern: '.yyyy-MM-dd-hh' },
        req: { type: "dateFile", filename: `${process.env.LOGGER_PATH}/webruntime/`, pattern: 'req-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true },
        login: { type: "dateFile", filename:  `${process.env.LOGGER_PATH}/loginlogs/`, pattern: 'login-yyyy-MM-dd.log', alwaysIncludePattern: true, compress: true }
    },
    categories: {
        default: { appenders: [ 'everything', 'req' ], level: 'info' },
        login: { appenders: [ 'everything', 'login' ], level: 'info' }
    }
});

exports.logger = log4js.getLogger();
exports.loginInfo = log4js.getLogger("login");
