/**
 * @description
 *  基础工具
 * 
 * @function
 *  test                        检测数据类型
 *  goto_url                    多环境兼容重定向
 * 
 *  verification_index_url      验证是否是需要 ssr 的url
 * 
 * 
 */

const test = (() => {

    const type_arr = [
        "Function",
        "Object",
        "Array",
        "Symbol",
        "Set",
        "Map",
        "WeakSet",
        "WeakMap",
        "String",
        "Number",
        "Boolean"
    ];

    const tmp = {};

    for ( const type_name of type_arr){
        tmp[`is${type_name}`] = (v) => {
            return Object.prototype.toString.call(v) === `[object ${type_name}]`;
        };
    }

    // isNumber 方法 - 追加条件
    const tmp_number = tmp["isNumber"];
    tmp["isNumber"] = (v) => tmp_number(v) && !Number.isNaN(v);

    return tmp;

})();


const goto_url = (res, url) => {
    if (!process.env.DEBUG) url = "/web" + url;
    return res.redirect(url);
};

const verification_index_url = (url) => {
    return /^\/$/.test(url) || /^\/(login|main|guideResetpwd)/.test(url);
};


module.exports = {
    test,
    goto_url,
    verification_index_url
}

