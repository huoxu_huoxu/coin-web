/**
 * @description
 *  common middlewares
 * 
 * 
 */

const Express = require("express");
const cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");

const log4js = require("log4js");
const { 
    logConfigure: {
        logger
    } 
} = require("../libs");

// parser req-body
const parserBody = Express.Router();
parserBody.use(bodyParser.urlencoded({ extended: false }));
parserBody.use(bodyParser.json());

// common middleware
const mw = Express.Router();

// ignore favicon.ico
mw.use((req, res, next) => {
    if(req.url === "/favicon.ico") return res.end();
    next();
});

// 记录接收到请求的时间
mw.use((req, res, next) => {
    req.__interface_started_at = Date.now();
    next();
});

// commmon
mw.use(cookieParser());

// get ip, token, body
mw.use((req, res, next) => {

    let __ip = req.headers["x-forwarded-for"] 
        ||  req.connection.remoteAddress 
        ||  req.socket.remoteAddress 
        ||  req.connection.socket.remoteAddress;

    // single
    Object.defineProperty(req, "ctx", {
        writable: false,
        configurable: false,
        enumerable: false,
        value: {
            __ip
        }
    });

    next();
});

mw.use(log4js.connectLogger(logger, {
    level: "auto",
    format: (req, res, format) => {

        // api transimission, ignore
        if (/^\/api\/.*/.test(req.url)){
            return ;
        }

        // output format
        let s = [
            `"ip ${req.ctx.__ip}"`,
            `":method ${req.path}"`,
            `-hs :status`,
            `-tc ${Date.now() - req.__interface_started_at}ms`
        ];
        return format(s.join(" "));
    }
}));


module.exports.mw = mw;
module.exports.parserBody = parserBody;
