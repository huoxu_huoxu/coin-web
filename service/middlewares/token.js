/**
 * @description
 *  verify token
 * 
 * 
 * @interface
 *  pageVeriifyToken            request page verify token
 *  apiVeriifyToken             request api verify token
 * 
 *  single_login                verify single login 
 * 
 */

const { o1100, o1200, o1202, o1204 } = require("../result");
const redis = require("../services/redisConnect");
const {
    user: {
        del_token_cached
    }
} = require("../controllers");
const { 
    cipher: {
        jwtVerify
    },
    tools: {
        goto_url,
        verification_index_url
    },
    logConfigure: {
        loginInfo
    }
} = require("../libs");


const single_login = async (id, token, res) => {
    let cached_token = await redis.hget(`user-${id}`, "token");
    cached_token = cached_token === null ? undefined : cached_token;
    if (!cached_token) return o1202;
    if (cached_token !== token) return o1200;
    return ;
};


module.exports.pageVeriifyToken = async (req, res, next) => {

    try {

        if (verification_index_url(req.url)){

            let __token = req.cookies.token;
            if (!__token && !/^\/$/.test(req.url)) return goto_url(res, "/");
            
            if (__token){
                let __userInfo = jwtVerify(req.cookies.token);
                if (!__userInfo && !/^\/$/.test(req.url)) return goto_url(res, "/");

                if (__userInfo){
                    let b_single_login = !!(await single_login(__userInfo.data.id, __token, res));
                    if (b_single_login && !/^\/$/.test(req.url)) return goto_url(res, "/");
    
                    __userInfo.b_single_login = b_single_login;

                    req.ctx.__token = __token;
                    req.ctx.__userInfo = __userInfo;
                    // record user automatic login
                    loginInfo.info("automaticLogin %s %s %s", req.ctx.__ip, __userInfo.data.id, req.url);

                }
            }

        }
    } catch (err){
        return next(err);
    }

    next();
};


module.exports.apiVeriifyToken = async (req, res, next) => {
    try {

        /**
         * The user is logged in,
         * token expires, prompt this user once. 
         * 
         * Users are not logged in,
         * display login page.
         * 
         */ 
        let { token: __token, logined } = req.cookies;
        if (!__token) {
            if (!logined) return res.json(o1204);
            res.cookie('logined', "", { maxAge: -1, httpOnly: true });
            return res.json(o1100);
        }

        let __userInfo = jwtVerify(req.cookies.token);
        if (!__userInfo) return res.json(o1100);

        let ret_single = await single_login(__userInfo.data.id, __token, res);
        if (ret_single) {
            /**
             * Compatible Safair, refresh VerifyUser page ComponentDidMount initial a request before redrict
             * You never know how stupid Chrome browser is.
             * Fuck Safair !
             * 
             */
            del_token_cached(res, null, __token, 1000);
            return res.json(ret_single);
        }

        req.ctx.__token = __token;
        req.ctx.__userInfo = __userInfo;

    } catch (err){
        return next(err);
    }
    next();
};


