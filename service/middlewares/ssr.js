/**
 * @description
 *  server send render 
 * 
 */
const fs = require("fs");
const path = require("path");
const zlib = require("zlib");
const url = require("url");

const redis = require("../services/redisConnect");
const DiySendServerRender = require("./__viewdata");
const {
    user: { create_verify_image }
} = require("../controllers");
const {
    tools: { goto_url, verification_index_url }
} = require("../libs");

// ssr
const ReactDOMServer = require('react-dom/server');
import React from 'react';
import { Container } from '../../static/src/js/views/router.js';
import ModuleLoading from "../../static/src/js/modules/moduleLoading/moduleLoading.js";
import { Provider } from "react-redux";
import { StaticRouter } from 'react-router-dom';
import { create_server_store } from "../../static/src/js/status/store";

// cached fs
let index_file = '';

module.exports = async function(req, res, next){

    try{

        if (verification_index_url(req.url)){

            let { ctx: { __ip, __userInfo, __token } } = req;
            let ssr_data, nS, fail_info;

            // guide user reset initial pwd, completed no get into page:guidePresetpwd
            if (__userInfo){
                let { data: { pwd }, b_single_login } = __userInfo;
                if (!/\/guideResetpwd/.test(req.url) && !b_single_login && pwd) return goto_url(res, "/guideResetpwd");
            }

            // parser front router
            ssr_data = await (new DiySendServerRender(req.url, __token)).run();
            // console.log("最后的STORE数据: ", ssr_data);


            // cached index.html
            if (process.env.DEBUG) index_file = "";
            if (!index_file){
                let sFile = fs.readFileSync(path.join(__dirname, "../../", "./public/index.html"));
                index_file = sFile.toString();
            }

            // render page
            let sReactModule = ReactDOMServer.renderToString(
                <Provider store={create_server_store(ssr_data)}>
                    <StaticRouter location={req.url} context={{}}>
                        <div>
                            <Container />
                            <ModuleLoading />
                        </div>
                    </StaticRouter>
                </Provider>
            );

            // generate store, replace html
            fail_info = { failure_times: +(await redis.hget(__ip, "failure_times")) };
            if (fail_info.failure_times > 2){
                let [ verify, captcha ] = create_verify_image();
                fail_info["v"] = verify;
                fail_info["img"] = captcha.data;
            }

            // render init
            nS = index_file.replace(/\<div id=\"app\"\><\/div\>/, `
                <div id="app">${sReactModule}</div><script>
                    window.__initialize_status = ${JSON.stringify(ssr_data)};
                    window.__server_render = ${JSON.stringify(fail_info)};
                </script>
            `);

            // gzip
            res.writeHead(200, {
                "Content-Type": "text/html; charset=utf8",
                "Content-Encoding": "gzip"
            });
            return res.end(zlib.gzipSync(nS));
        }

    }catch(err){
        console.log("[ssr loading failure], %s", err.toString());
    }

    next();

};