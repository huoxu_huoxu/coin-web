/**
 * @description
 *  ssr 所需 store 生成
 * 
 * @desc
 *  reg                         对应页面的正则集合, 获取关键信息
 * 
 * 
 * @function
 *  reqFunc                     发起请求封装, 凡是发生错误, 全部 throw Error
 * 
 * @class
 *  DiySendServerRender         ssr - 分析路由、数据获取、生成首屏状态机
 * 
 * @attribute
 *  __init_store                初始化状态机
 * 
 * 
 */

const url = require("url");
const axios = require("axios");


const reg = {
    choice:     /\/accounts\/choice\/(\d+)$/,
    detail:     /\/detail\/(\d+)$/,
    baskets:    /\/baskets\/(\d+)$/,
    addBasket:  /\/addBasket\/(\d+)$/,
    accounts:   /\/accounts\/(\d+)\/(\d+)$/
};

console.log("对接", process.env.server_api, "...");

const reqFunc = async (url, token, method = "GET", params = {}) => {
    method = method.toLowerCase();
    let sUrl = `${process.env.server_api}${url}`;
    params = { ...params, token };

    let s = '';
    if(method === "get" || method === "delete"){
        for(let [key, name] of Object.entries(params)){
            if(!s){
                s += `?${key}=${name}`;
                continue;
            }
            s += `&${key}=${name}`;
        }
        sUrl += s;
        params = {};
    }

    try {
        let { data } = await axios[method](sUrl, params);
        if (!data) throw Error("error");
        if (data.errcode !== 0) throw Error("error");
        return data;
    } catch (err){
        console.log("请求业务服务器, 发生了错误", err.toString());
        throw Error("error");
    }
};

// 基础数据组成
const __init_data = async (reg_name, __store, pathname, token, b) => {
    let { data: { list } } = await reqFunc("api/data/getAccounts", token);
    let [ , account_id ] = pathname.match(reg[reg_name]);

    account_id = +account_id;
    let [ account ] = list.filter(v => v.id === account_id);
    if (!account) account = list[0];

    // 生成 accounts 列表信息
    if (b){
        let tmp = {};
        for (let item of list){
            tmp[item.id] = item;
        }
        __store.accounts = tmp;
    }

    // 生成当前用户信息
    __store.userInfo = {
        account_display_name:account["display_name"],
        account_id: account["id"]
    }

    return __store;
};

// 主内容
const __main = async (__store, pathname, token) => {


    // 账户列表
    if (reg["choice"].test(pathname)){
        __store.header.current_status = "accounts";
        return await __init_data("choice", __store, pathname, token);
    }

    // 账户详情
    if (reg["detail"].test(pathname)){
        __store.header.current_status = "accounts_detail";
        return await __init_data("detail", __store, pathname, token);
    }


    
    // 篮子列表
    if (reg["baskets"].test(pathname)){
        return await __init_data("baskets", __store, pathname, token, true);
    }

    // 添加篮子
    if (reg["addBasket"].test(pathname)){
        return await __init_data("addBasket", __store, pathname, token, true);
    }


    // 订单列表
    if (reg["accounts"].test(pathname)){

        let [ , account_id ] = pathname.match(reg["accounts"]);
        __store = await __init_data("accounts", __store, pathname, token, true);

        // 篮子列表
        let { data: { page_index: current, page_size: pageSize, total, list } } = await reqFunc(`api/data/getBaskers/${account_id}`, token);
        let baskets_tmp = {};
        for (let basket of list) {
            baskets_tmp[basket.id] = basket;
        };

        __store.baskets.map = baskets_tmp;
        __store.baskets.pager = {
            total,
            current,
            pageSize
        };

        return __store;
    }

};


class DiySendServerRender {

    constructor (s, token){
        this.url = url.parse(s);
        this.token = token;
    }

    __init_store = {
        loadingState: true,
        accounts: {},
        userInfo: {
            account_display_name: "",
            account_id: 0
        },
        baskets: {
            map: {},
            pager: {},
            cond: {}
        },
        header: {
            current_status: "baskets"
        }
    }

    // 生成存储器
    __generate ({ pathname }, token){
        if (/\/main\//.test(pathname)) return __main(this.__init_store, pathname, token);
        return false;
    }

    async run (){

        let { pathname } = this.url;

        // 根
        if (/^\/$/.test(pathname)) return this.__init_store;
        
        // 预置 - 过渡部分
        if (/\/main(\/accounts)?$/.test(pathname)) return this.__init_store;

        // 引导用户修改密码
        if (/guideResetpwd/.test(pathname)) return this.__init_store;

        try {
            return await this.__generate(this.url, this.token);
        } catch (err){
            console.log("拉取数据, 请求业务服务器, 发生了错误", err.toString());
            return false;
        }

    }

}


module.exports = DiySendServerRender;
