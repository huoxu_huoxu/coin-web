/**
 * @description
 *  防护
 * 
 * @attribute
 *      ip_white                ip白名单
 *      ip_black                ip黑名单
 * 
 * 
 * @function
 *      access_restrictions     访问限制
 * 
 * 
 * 
 */

const redis = require("../services/redisConnect");
const { o5900, o5902 } = require("../result");

const ip_white = [
    "::1"
];

const ip_black = [

];


// 1s max req equal 3
const times = 1;
const max = 4;

module.exports.access_restrictions = async (req, res, next) => {

    let { ctx: { __ip } } = req;

    // 黑名单
    let b_black = ip_black.includes(__ip);
    if (b_black) {
        return res.status(403).json(o5902);
    }       

    // 白名单
    let b_white = ip_white.includes(__ip);
    if (b_white) return next();

    // 限制
    let s = `${__ip}-limit-${parseInt(Date.now() / 1000)}`;
    let ret = await redis.get(s);
    if (ret && ret > max) return res.json(o5900);

    // 记录
    if (!ret){
        await redis.setex(s, times, 1);
    } else {
        await redis.incr(s);
    }

    next();
};


