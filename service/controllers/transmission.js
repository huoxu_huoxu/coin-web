/**
 * @description
 *  透传, 将业务级接口发给业务服务器处理
 * 
 * @function
 *  bindError                   统一错误处理
 *  transparentTransmission     透传主体
 *  generateReqHead             生成透传的头 
 * 
 * 
 */

const http = require("http"); 
const https = require("https")

// init service info
const reg = /(localhost|[\w\.]+\.com|[\d\.]+):?(\d+)?(\/.*)/;
let target, prepositionPath;
{
    let ret = process.env.server_api.match(reg);
    if (!ret){
        throw Error("无法解析service的hostname");
    }
    target = {
        hostname: ret[1],
        port: ret[2] || 80
    };
    prepositionPath = ret[3];
}

const generateReqHead = (req) => {

    let path = req.url;
    if (prepositionPath){
        path = prepositionPath + req.url;
        path = path.replace(/(\/)\1+/, "$1");
    }

    if (/\?/.test(path)){
        path += `&token=${req.ctx.__token}`;
    } else {
        path += `?token=${req.ctx.__token}`;
    }

    delete req.headers["cookie"];
    return Object.assign({}, target, {
        method: req.method,
        headers: req.headers,
        path
    });

};

const bindError = (next) => {
    return (err) => {
        let msg = String(err.stack || err);

        console.log("发生错误, %s", msg);
        let error = new Error("抱歉, 当前服务器繁忙, 请稍后再试");
        next(error);
    };
}

const transparentTransmission = (req, res, next) => {

    if (/^\/api\/.*/.test(req.url)){
        let info = generateReqHead(req);

        var tmpProtocol = http;
        // if(req.protocol === "https") {
        //     info.port = "443";
        //     tmpProtocol = https;
        // }
        if (!process.env.DEBUG) {
            info.port = "443";
            info.rejectUnauthorized = false;
            tmpProtocol = https;
        }

        let req2 = tmpProtocol.request(info, (res2) => {
            res2.on("error", bindError(next));
            res.writeHead(res2.statusCode, res2.headers);
            res2.pipe(res);
        });
        req.pipe(req2);
        req2.on("error", bindError(next));

        return ;
    }

    next();

};

module.exports = {
    transparentTransmission
};
