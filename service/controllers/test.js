/**
 * @description
 *  测试
 * 
 * @function
 *  testGetVG           获取图片验证的结果
 *  tChangePasswd       直接通过id改密码的后门
 * 
 */
const { succ, o1014, o1016 } = require("../result");

const { 
    cipher: { createMd5 }
} = require("../libs");
const {
    models: { Users }
} = require("../models");
const {
    create_verify_image,
    del_token_cached
} = require("./user");

const uncahngedSalt = "poi_089";

module.exports.testGetVG = (req, res, next) => {
    try {
        let [ verify, captcha ] = create_verify_image();
        return res.json({ v: verify, t: +captcha.text });
    } catch (err){
        next(err);
    }
};

module.exports.tChangePasswd = async (req, res, next) => {
    try {

        let { cipher, id, password } = req.body;
        if (cipher !== "qscfv") return res.json(o1014);

        let item = await Users.once({ id }, "*");
        if (!item) return res.json(o1016);

        del_token_cached(res, id);

        let passwd = createMd5(password);
        passwd = createMd5(uncahngedSalt + passwd + item.salt);
        await Users.update({ password: passwd }, { id });

        return res.json(succ);

    } catch (err){
        next(err);
    }
};



