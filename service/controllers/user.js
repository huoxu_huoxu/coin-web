/**
 * @description
 *  用户相关
 * 
 * 
 * @interface
 *  addUser             添加（注册）平台账号
 *  login               登录账号
 *  changePassword      修改密码
 *  getUsers            仅限超级管理员用户调用
 *  destoryToken        退出, 清除cookie
 * 
 * 
 * @function
 *  create_verify_image     创建验证用的SVG图片
 *  login_failure           登录失败的统一处理
 *  set_token_cached        针对 用户id 与 token redis内设置缓存, cookie中记录
 *  del_token_cached        清除上面的
 * 
 * 
 * @attribute
 *  uncahngedSalt           固定盐
 *  times                   令牌有效时间
 *  verify_image_times      验证图片有效时间 
 *  imageKey                图片jwt加密字段
 * 
 * 
 */

const svgCaptcha = require("svg-captcha");
const { o1000, succ, o1004, o1006, o1008, o1010, o1012, o1016 } = require("../result");
const { 
    cipher: { createMd5, jwtVerify, jwtSign },
    logConfigure: { loginInfo }
} = require("../libs");
const {
    models: { Users }
} = require("../models");
const { QUERY } = require("../services/mysqlConnect");
const redis = require("../services/redisConnect");


const uncahngedSalt = "poi_089";
const imageKey = "mnbvc";

let times, verify_image_times;

{
    let i = 60 * 60;
    if (process.env.DEBUG){
        times = 1 * i;
        verify_image_times = 30;
    } else {
        times = 3 * i;
        verify_image_times = 60;
    }
}

const create_verify_image = () => {
    let captcha = svgCaptcha.createMathExpr({ fontSize: 50, width: 100, height: 60 });
    return [ jwtSign(verify_image_times, +captcha.text, imageKey), captcha ];
};

const login_failure = async (req, res, err_info) => {
    let { ctx: { __ip } } = req;
    await redis.hincrby(__ip, "failure_times", 1);
    let v = await redis.hget(__ip, "failure_times");
    if (v > 2){
        let [ verify, captcha ] = create_verify_image();
        return res.json(Object.assign({}, err_info, { data: { v: verify, img: captcha.data, failure_times: +v } } ));
    }
    return res.json(Object.assign({}, err_info, { data: { failure_times: v } }));
};

const set_token_cached = (res, id, token) => {
    redis.hset(`user-${id}`, "token", token);
    res.cookie('token', token, { 
        maxAge: times * 1000,
        httpOnly: true
    });
    res.cookie('logined', true, { httpOnly: true });
};

const del_token_cached = (res, id, token = "", maxAge = -1) => {
    if (id) redis.hdel(`user-${id}`, "token");
    res.cookie("token", token, {
        httpOnly: true,
        maxAge
    });
    res.cookie('logined', "", { maxAge: -1, httpOnly: true });
};



module.exports.addUser = async (req, res, next) => {
    try {

        let { email, password, nickname, cipher } = req.body;
        if (cipher !== "qscfv") return res.json(o1012);

        let nums = await Users.is_exists({email: email});
        if (nums) return res.json(o1000);

        let salt = (Math.round(Math.random() * 10000000)).toString();
        let passwd = createMd5(uncahngedSalt + password + salt);
        
        await Users.insert({
            password: passwd,
            nickname, 
            email,
            salt
        });

        return res.json(succ);

    } catch (err){
        next(err);
    }
};

module.exports.login = async (req, res, next) => {
    try {

        let { email, password, verifyValue, verifyToken } = req.body;

        // 验证是否需要验证图片
        let __ip = req.ctx.__ip;
        let ret = +(await redis.hget(__ip, "failure_times"));
        if (ret && ret > 2){
            let resul = jwtVerify(verifyToken, imageKey);
            if (!resul) return login_failure(req, res, o1004);
            if (resul["data"] !== verifyValue) return login_failure(req, res, o1006);
        }

        // failure
        let user = await Users.once({ email }, "*");
        if (!user) return login_failure(req, res, o1008);
        if (createMd5(uncahngedSalt + password + user.salt) !== user.password) return login_failure(req, res, o1010);


        // ---- succ

        // token format
        let token = { id: user.id };

        // using init password
        if (password === "d47bd33854a15e8ec9e0819416db2507") token["pwd"] = true;

        token = jwtSign(times, token);

        // update redis info and set cookie
        redis.hdel(__ip, "failure_times");
        set_token_cached(res, user.id, token);

        // record user login
        loginInfo.info("login %s %s %s %s", __ip, user.id, user.nickname, user.email);

        return res.json(Object.assign({}, succ, {
            data: {
                nickname: user.nickname,
                email: user.email,
                role: user.role
            }
        }));

    } catch (err){
        next(err);
    }
};

module.exports.changePassword = async (req, res, next) => {

    try {

        let { password } = req.body;
        let { data: { id } } = req.ctx.__userInfo;

        let item = await Users.once({ id }, "*");
        if (!item) return res.json(o1016);

        let passwd = createMd5(uncahngedSalt + password + item.salt);
        await Users.update({ password: passwd }, { id });

        // succ
        let token = jwtSign(times, { id: item.id });
        set_token_cached(res, item.id, token);

        return res.json(Object.assign({}, succ, {
            data: { token }
        }));

    } catch (err){
        next(err);
    }

};

module.exports.automaticLogin = async (req, res, next) => {
    try {
        // record user automatic login
        loginInfo.info("automaticLogin %s %s %s", req.ctx.__ip, req.ctx.__userInfo.data.id, "/");
        return res.json(succ);
    } catch (err){
        next(err);
    }
};


module.exports.getUsers = async (req, res, next) => {

    try {

        let resul = await Users.all({}, [ "id", "nickname" ]);
        for (let user of resul){
            let sql = `
                select id, display_name from accounts a right join (
                    select account_id from account_user where user_id = ?
                ) au on a.id = au.account_id
            `;
            let accounts = await QUERY( sql, [ user.id ] );
            user.accounts = accounts;
        }

        return res.json(Object.assign({}, succ, {
            data: {
                list: resul
            }
        }));

    } catch (err){
        next(err);
    }

};


module.exports.verifyImage = async (req, res, next) => {
    try {

        let [ verify, captcha ] = create_verify_image();
        return res.json(Object.assign({}, succ, { data: { v: verify, img: captcha.data } } ));

    } catch (err){
        next(err);
    }
};

module.exports.destoryToken = async (req, res, next) => {
    try {

        // --- del redis and clean cookie
        let { data: { id } } = req.ctx.__userInfo;

        del_token_cached(res, id);
        return res.json({ errcode: 0, msg: "succ"});
    } catch (err){
        next(err);
    }
};


module.exports.create_verify_image = create_verify_image;
module.exports.del_token_cached = del_token_cached;
module.exports.set_token_cached = set_token_cached;
