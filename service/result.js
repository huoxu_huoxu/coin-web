/**
 * @description
 *  返回 errcode 集合
 *  
 * @desc
 *  o1xxx                验证类错误
 * 
 *  o4xxx                请求端错误
 *  
 *  o5xxx                服务器端写入错误
 * 
 */


const verifyError = {
    o1000: {
        errcode: 1000,
        msg: "email已注册过, 请勿重复注册"
    },
    o1004: {
        errcode: 1004,
        msg: "验证图片过期"
    },
    o1006: {
        errcode: 1006,
        msg: "验证码错误"
    },
    o1008: {
        errcode: 1008,
        msg: "账号 / 邮箱 不存在"
    },
    o1010: {
        errcode: 1010,
        msg: "账号或密码错误"
    },
    o1012: {
        errcode: 1012,
        msg: "非法注册"
    },
    o1014: {
        errcode: 1014,
        msg: "非法操作"
    },
    o1016: {
        errcode: 1016,
        msg: "不存在用户"
    },
    o1100: {
        errcode: 1100,
        msg: "登录过期"
    },
    o1200: {
        errcode: 1200,
        msg: "您的账户, 已在其他设备上登录, 请重新登录"
    },
    o1202: {
        errcode: 1202,
        msg: "请重新登录"
    },
    o1204: {
        errcode: 1204,
        msg: "无"
    }
};

const serviceError = {
    o5900: {
        errcode: 5900,
        msg: "请求过于频繁"
    },
    o5902: {
        errcode: 5902,
        msg: "服务器拒绝访问"
    }
};


const resul = Object.assign({
    succ: {
        errcode: 0, 
        msg: "succ" 
    }
}, verifyError, serviceError);

module.exports = resul;
