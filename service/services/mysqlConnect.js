/**
 *  @readme
 *      连接mysql
 *      
 *      QUERY: 通用
 *      AFFAIR: 事务
 * 
 */

const mysql = require('mysql');
const mysql_config = {
    connectionLimit: 10,
    host: process.env.DB_HOST,
    post: process.env.DB_PORT,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    multipleStatements: true
};
let pool = mysql.createPool(mysql_config);

// 通用语句
const QUERY = function (...argv){
    return new Promise(function(resolve, reject){
        pool.getConnection((err, conn) => {
            if(err){
                err.status = 12001;
                throw err;
            }
            conn.query(...argv, function(err, rows, fields){
                conn.release();
                if(err){
                    err.status = 12002;
                    reject(err);
                    return ;
                }
                resolve(rows);
            });
        });
    });
};


// model 使用
const MODEL_QUERY = function (conn){
    if (conn){
        return (...argv) => {
            return new Promise((resolve, reject) => {
                conn.query(...argv, (err, rows, fields) => {
                    if (err){
                        err.status = 12010;
                        reject(err);
                        return ;
                    }
                    resolve(rows);
                });
            });
        };
    }
    return QUERY;
}; 


// 事务
const AFFAIR = async () => {
  
    let connect = await new Promise(resolve => {
        pool.getConnection((err, conn) => {
            if (err) {
                err.status = 12101;
                throw err;
            }
            resolve(conn);
        }); 
    });

    let end = {
        value: undefined,
        done: true
    };  

    return {
        next: (...argv) => {
            if ( !argv.length ) {
                connect.release();
                return end;
            }
            let str = argv[0];
            return new Promise((resolve, reject) => {
                connect.query(...argv, (err, rows, fields) => {
                    if (err) {
                        return reject(err);
                    }
                    if ( str === "rollback" || str === "commit" ){
                        connect.release();
                        return resolve(end);
                    }
                    let ret = {
                        value: rows,
                        done: false
                    };
                    resolve(ret);
                });
            });
        }
    };

};



module.exports = {
    QUERY,
    AFFAIR,
    MODEL_QUERY
};

