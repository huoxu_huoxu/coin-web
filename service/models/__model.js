/**
 * @description
 *  数据 - model层 
 * 
 * 
 * @class Model
 *  
 *  @param   
 *      models              数据表模型
 *      name                数据表名称
 *      __maxLength         string型, 默认最长值
 *      __minLength         string型, 默认最短值    
 *  
 *  @function
 *      __testType          传入执行sql的字段, 进行类型验证     
 *      __parseParams       抽离 key-value 形成sql语句形式（key=?, ...）
 *      __generate_insert   生成insert的sql语句
 *      __select            生成基础select的带倒序查询的sql语句
 * 
 * 
 *      is_exists           是否存在数据表中
 *      insert              插入一条数据
 *      once                获取数据表中符合要求的第一个
 *      all                 获取数据表中所有符合要求的
 *      update              更新数据
 *      base_sql            生成基础的select的sql语句
 * 
 * 
 */

const { test } = require("../libs/tools");
const { QUERY } = require("../services/mysqlConnect");


class Model {

    constructor (name, models = {}){
        this.models = models;
        this.name = name;

        this.__maxLength = 255;
        this.__minlength = 6;
    }

    // insert, update 时检查字段
    __testType (params, b){
        if (!test.isObject(params)) throw Error("传递参数存在错误");
        for (let [k, v] of Object.entries(this.models)){
            let item = params[k];
            
            // 必填字段是否缺失, 只有在insert情况下才检查必填, update必填项可以不更新
            if (!b){
                if (item === undefined && v.required) throw Error(`缺失, 必填字段 ${k}`);
            }
            
            // 非必填字段, 不存在就过
            if (!item && !Number.isNaN(item)) continue;

            // 字段类型是否正确
            let sType = v.type.substring(0, 1).toUpperCase() + v.type.substr(1);
            if (!test[`is${sType}`](item)) throw Error(`字段 ${k} 类型不正确, 应该是 ${v.type}`);

            // string 类型做长度检验
            if (v.type === "string"){
                let maxLength = v.maxLength === undefined ? this.__maxLength : v.maxLength;
                let minLength = v.minLength === undefined ? this.__minLength : v.minLength;

                if (maxLength !== null){
                    if (item.length > maxLength) throw Error(`字段 ${k} 最大长度不能超过 ${maxLength}`);
                }
                if (item.length < minLength) throw Error(`字段 ${k} 最小长度不能小于 ${minLength}`);
            }
        }
    }

    // 抽离的 k - v 映射
    __parseParams (params, rule = "=?"){
        let options = [], tmp = [];
        for (let [k, v] of Object.entries(params)){
            if (v !== undefined){
                tmp.push(`${k}${rule}`);
                options.push(v);
            }
        }
        return [ options, tmp ];
    }

    async is_exists (params){
        let options = [], tmp, s;
        if (test.isObject(params)){
            [ options, tmp ] = this.__parseParams(params);
            s = tmp.join(" and ");
        } else {
            s = `id = ?`;
            options.push(params);
        }

        let [ { nums } ] = await  QUERY(
            `select count(*) as nums from ${this.name} where ${s}`,
            options
        );

        return nums;
    }

    __generate_insert (params){

        // 数据类型检测
        this.__testType(params);

        let [ options, tmp ] = this.__parseParams(params, "");
        let oDate = new Date();
        options.push(oDate, oDate);
        tmp.push("created_at", "updated_at");
        let s = tmp.join(",");
        s = `insert into ${this.name} (${s}) values (${Array(options.length).fill("?").join(",")})`;
        return [ s, options ];
    }

    async insert (params){

        // 数据类型检测
        this.__testType(params);

        let [ s, options ] = this.__generate_insert(params);
        let { insertId: id } = await QUERY( s, options );
        return id;
    }

    __select (params, needs, sort, option){

        let [ options, tmp ] = this.__parseParams(params);
        let s = tmp.join(' ' + option + ' ');

        if (test.isArray(needs)){
            needs = needs.join(",");
        }
        return QUERY(
            `select ${needs} from ${this.name} where ${s} order by id ${sort} `,
            options
        );
    }

    async once (params, needs = "id", option = "and"){
        
        let [ item ] = await this.__select(params, needs, "asc", option);

        return item;
    } 

    async all (params, needs = "id", sort = "asc", option = "and"){
        if (JSON.stringify(params) === "{}"){
            if (test.isArray(needs)){
                needs = needs.join(",");
            }
            return QUERY(`select ${needs} from ${this.name}`);
        }
        return this.__select(params, needs, sort, option);
    }

    async total_nums (s, opts){

        let a = s.split(" ");
        let i_from = a.indexOf("from");
        let i_select = a.indexOf("select");
        
        a.splice(i_select + 1,  i_from - i_select - 1, "count(*) as nums");
        s = a.join(" ");

        let [ { nums } ] = await QUERY(s, opts);
        return nums;
    }

    async update (params, cond, option = "and"){

        // 数据类型检测
        this.__testType(params, true);
        
        // 更新的字段
        let [ options, tmp ] = this.__parseParams(params);
        let oDate = new Date();
        options.push(oDate);
        tmp.push("updated_at=?");
        let s = tmp.join(" , ");

        // 条件
        let conds;
        [ conds, tmp ] = this.__parseParams(cond);
        let s2 = tmp.join(' ' + option +' ');

        return QUERY(
            `update ${this.name} set ${s} where ${s2}`,
            options.concat(conds)
        );

    }

    base_sql (params, needs, option = "and"){
        let [ options, tmp ] = this.__parseParams(params);
        let s = tmp.join(' ' + option + ' ');

        if (test.isArray(needs)){
            needs = needs.join(",");
        }

        return [
            `select ${needs} from ${this.name} where ${s} `,
            options
        ];
    }

}

module.exports = Model;
