/**
 * @description
 *  表的模型实例
 * 
 * @default
 *  type string minLength 6, maxLength 255
 * 
 */

const Model = require("./__model");

const Users = new Model("users", {
    parent_id: {
        type: "number"
    },
    nickname: {
        type: "string",
        required: true
    },
    email: {
        type: "string",
        required: true
    },
    password: {
        type: "string",
        required: true
    },
    salt: {
        type: "string",
        required: true,
        minLength: 1
    },
    role: {
        type: "number"
    }
});

const Accounts = new Model("accounts", {
    display_name: {
        type: "string",
        required: true
    },
    name: {
        type: "string",
        required: true
    },
    exchange: {
        type: "string",
        required: true,
        minLength: 2
    }
});

const AccountUser = new Model("account_user", {
    user_id: {
        type: "number",
        required: true
    },
    account_id: {
        type: "number",
        required: true
    }
});


module.exports = {
    Users,
    Accounts,
    AccountUser
};

