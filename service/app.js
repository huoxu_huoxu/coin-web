// app - service
const path = require("path");
const Express = require("express");

const Router = Express.Router;
const app = Express();

// controllers
const {
    transimission: {
        transparentTransmission
    },
    user: {
        login,
        addUser,
        changePassword,
        getUsers,
        destoryToken,
        automaticLogin,
        verifyImage
    },
    test: {
        testGetVG,
        tChangePasswd
    }
} = require("./controllers");

// middlewares
const { 
    ssr, 
    common: {
        mw,
        parserBody
    },
    token: {
        pageVeriifyToken,
        apiVeriifyToken
    },
    secure: {
        access_restrictions
    }
} = require("./middlewares");
{
    
    app.use(mw);
    app.use(pageVeriifyToken);
    app.use(ssr);

    // TEST-ENV status public
    {
        process.env.DEBUG && app.use(Express.static(path.resolve(__dirname, "../", "public")));
    }
}

// using controllers
{

    const   user = Router(),
            superAdmin = Router(),
            baskDoor = Router();

    // test req-params
    const ver = Router();
    ver.use(access_restrictions, apiVeriifyToken);


    // !@ old token
    app.get("/webapi/destroyToken", ver, destoryToken);


    // !@ user
    user.get("/webapi/user", ver, automaticLogin);
    user.post("/webapi/user", access_restrictions, parserBody, login);
    user.put("/webapi/user", ver, parserBody, changePassword);
    user.get("/webapi/user/verifyImage", access_restrictions, verifyImage);
    app.use(user);


    // !@ superamin
    superAdmin.get("/webapi/users", ver, getUsers);
    app.use(superAdmin);


    // !@ back door
    baskDoor.post("/webapi/user/register", parserBody, addUser);

    baskDoor.get("/webapi/verify/testGetVG", testGetVG);
    baskDoor.put("/webapi/user/tChangePasswd", parserBody, tChangePasswd);
    app.use(baskDoor);


    // !@ proxy business
    app.use(ver, transparentTransmission);
}


// 404
app.use((req, res, next) => {
    let error = new Error("not found");
    error.status = 404;
    next(error);
});

// 500
app.use((err, req, res, next) => {
    res.json({
        errcode: err.status || 500,
        msg: err.toString()
    });
    next();
});


module.exports = app;
