/**
 * @description
 *  pm2 + babel-node 合用方案
 * 
 */

const childProcess = require('child_process');

const options = {
    stdio: [0, 1, 2]
};

{
    childProcess.execSync(`babel-node ./service/index.js`, options);
}

