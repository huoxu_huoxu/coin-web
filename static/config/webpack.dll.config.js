const path =                        require("path");
const os =                          require("os");
const fs =                          require("fs");
const webpack =                     require("webpack");
const config =                      require("../config");
const CleanWebpackPlugin =          require("clean-webpack-plugin");


// 动态记录dll
const dll_name = `${Math.round(Math.random()*10000000)}.dll.js`;
const file = fs.readFileSync(path.resolve(__dirname, "../", "config.js")).toString();
const lines = file.split(os.EOL);
for (let i in lines){
    let line = lines[i];
    if (line && /config\.DLL_NAME/.test(line)){
        lines[i] = `config.DLL_NAME = \"react.${dll_name}\"`;
        break;
    }
}

fs.writeFileSync(path.resolve(__dirname, "../", "config.js"), lines.join(os.EOL));


module.exports = {
    mode: "production",
    entry: {
        react: [
            'react', 
            'react-dom', 
            'react-redux', 
            'react-router', 
            'react-router-dom',
            'redux',
            'react-middleware-async',
            'lodash',
            'axios',
            'antd',
            'blueimp-md5'
        ]
    },
    output: {
        // 动态链接库输出的文件名称
        filename: `./dist/[name].${dll_name}`,
        // 输出路径
        path: path.resolve(__dirname, `../../${config.PUBLIC_NAME}`),
        // 链接库(react.dll.js)输出方式 默认'var'形式赋给变量 b
        libraryTarget: 'var',               
        // 全局变量名称 导出库将被以var的形式赋给这个全局变量 通过这个变量获取到里面模块
        library: '_dll_[name]_[hash]'       
    },
    plugins: [
        new webpack.DllPlugin({
            // path 指定manifest文件的输出路径
            path: path.join(__dirname, `../../${config.PUBLIC_NAME}`, 'dist', '[name].manifest.json'),
            // 和library 一致，输出的manifest.json中的name值
            name: '_dll_[name]_[hash]', 
        }),
        new CleanWebpackPlugin(
            [ "public/dist/react.*" ],
            {
                //根目录
                root: path.resolve(__dirname, "../../"),
                //开启在控制台输出信息       　　　　　　　　　　
                verbose: true,
                //启用删除文件        　　　　　　　　　　
                dry: false        　　　　　　　　　　
            }
        )
    ]
}

