// 动作处理处理器


// 加载状态 处理器
const LOADING_REDUCER = (state = true, action) => {
    switch(action.type){
        case "loading_show": return true;
        case "loading_hide": return false;
        default: return state;
    }
};

// 导航
const NAV_REDUCER = (state = {}, action) => {
    switch (action.type){
        case "update_nav_status": 
            let n = _.clone(state);
            n["current_status"] = action.current_status;
            return n;
        default: return state;
    }
};


// 获取 accounts 处理器
const SET_ACCOUNTS = (state = {}, action) => {
    switch (action.type){
        case "get_accounts": {
            let { errcode, data } = action.async;
            let n = state;
            if (errcode === 0){
                n = {};
                for (let item of data.list){
                    n[item.id] = item;
                    item['key'] = item.id;
                }
            }  
            return n;
        }
        default: return state;
    }
};


// 设置 用户选中 account 处理器
const SET_ACCOUNT = (state = {}, action) => {
    switch (action.type){
        case "set_account_name":{
            let copy_state = _.cloneDeep(state);
            copy_state.account_id = action.account["id"];
            copy_state.account_display_name = action.account["display_name"];
            return copy_state;
        }
        default: return state;
    }
};

// 获取 baskets 处理器
const SET_BASKETS = (state = {}, action) => {
    switch (action.type){
        case "get_baskets": {
            let { errcode, data } = action.async;
            let n = _.cloneDeep(state);
            if (!errcode){
                let { list, total, page_index: current, page_size: pageSize } = data;
                n['map'] = {};
                for (let item of list){
                    n["map"][item.id] = item;
                    item['key'] = item.id;
                }
                n["pager"] = {
                    total,
                    current,
                    pageSize,
                };
            }  
            return n;
        }
        case "save_search_cond": {
            let n = state;
            if (JSON.stringify(state['cond']) !== JSON.stringify(action.cond)){
                n = _.cloneDeep(state);
                n["cond"] = action.cond;
            }
            return n;
        }
        case "ready_basket": {
            let { errcode, data } = action.async;
            let n = state;
            if (!errcode){
                n = _.cloneDeep(state);
                n["map"][data["basket_id"]]["status"] = "new";
            }
            return n;
        }
        case "rename_basket": {
            let { errcode, data } = action.async;
            let n = state;
            if (!errcode){
                if (data["filename"] !== n["map"][data["basket_id"]]["filename"]){
                    n = _.cloneDeep(state);
                    n["map"][data["basket_id"]]["filename"] = data["filename"];
                }
            }
            return n;
        }
        case "do_opt_basket": {
            let { errcode, data } = action.async;
            let n = state;
            if (!errcode){
                n = _.cloneDeep(state);
                n["map"][data["basket_id"]]["do_opt"] = data["do_opt"];
                n["map"][data["basket_id"]]["status"] = data["status"];
            }
            return n;
        }
        case "delete_basket": {
            let { errcode , data } = action.async;
            let n = state;
            if (!errcode){  
                n = _.cloneDeep(state);
                delete n["map"][data["basket_id"]];
            }
            return n;
        }
        case "clear_baskets": {
            return { map: {}, pager: {}, cond: {} };
        }
        default: return state;
    }
};


export {
    SET_ACCOUNTS,
    LOADING_REDUCER,
    SET_ACCOUNT,
    SET_BASKETS,
    NAV_REDUCER
};

