// 状态容器

import { bindActionCreators } from 'redux';

// 引入动作构造器

import { 
    GET_ACCOUNTS,
    SET_ACCOUNT_NAME,
    GET_BASKETS,
    READY_BASKET,
    RENAME_BASKET,
    UPDATE_BASKET,
    NAV_STATUS,
    SAVE_SEARCH_COND,
    CLEAR_BASKETS,
    DELETE_BASKET,
} from './action.js';


// accounts - table 账户列表
const accounts_state =(state, ownProps) => {
    return {
        accounts: state.accounts,
        userInfo: state.userInfo
    }
};

const accounts_dispatch = (dispatch, ownProps) => {
    return bindActionCreators({
        get_data: GET_ACCOUNTS,
        nav_status: NAV_STATUS,
    }, dispatch);
};

export let accounts_container = {
    state: accounts_state,
    props: accounts_dispatch
};


// account - header 选中账户后在右上角显示账户名称
const header_attrs = (state, ownProps) => {
    return {
        user_info: state.userInfo,
        header: state.header,
        accounts: state.accounts
    };
};

export let header_status = {
    state: header_attrs
};

// account - baskets 篮子信息
const baskets_state = (state) => {
    return {
        baskets: state.baskets,
        accounts: state.accounts
    }
};

const baskets_dispatch = (dispatch) => {
    return bindActionCreators({
        get_data: GET_BASKETS,
        set_account: SET_ACCOUNT_NAME,
        ready_basket: READY_BASKET,
        rename_basket: RENAME_BASKET,
        update_do_opt: UPDATE_BASKET,
        nav_status: NAV_STATUS,
        save_search_cond: SAVE_SEARCH_COND,
        clear_baskets: CLEAR_BASKETS,
        delete_basket: DELETE_BASKET
    }, dispatch);
};

export let baskets_container = {
    state: baskets_state,
    props: baskets_dispatch
};


// super admin - 权限
const super_state = (state) => {
    return {}
};
const super_dispatch = (dispatch) => {
    return bindActionCreators({
        nav_status: NAV_STATUS,
    }, dispatch);
};

export let super_container = {
    state: super_state,
    props: super_dispatch
};



