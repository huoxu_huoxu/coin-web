
// 动作构建器

const { 
    requestFunc,
    urlSynthesis,
    create_url
} = require("../libs/base");
const { 
    getAccounts,
    getBaskets,
    getOrders,
    downloadBasket,
    updateOrder,
    basketApi,
    superGetUsers,
    superExportOrders,
    superExportSettlementPrice,
    changePasswd,
    verifyImage,
    downloadAccountBalance
} = require("../requestApi");  

import STORE from "./store";
import { setTimeout } from "timers";

// 动作 - 加载状态
const LOADING_SHOW = () => ({type: "loading_show"});
const LOADING_HIDE = () => ({type: "loading_hide"});

// 动作 - 更新导航状态
const NAV_STATUS = (status) => ({
    type: "update_nav_status",
    current_status: status
});


// 动作 - 获取account列表
const GET_ACCOUNTS = () => ({
    type: "get_accounts",
    before: LOADING_SHOW,
    after: LOADING_HIDE,
    async: new Promise(async resolve => {
        let resul = await requestFunc(getAccounts, "GET");
        resolve(resul);
    })
});


// 动作 - 设置右上角 账户名称
const SET_ACCOUNT_NAME = account => ({
    type: "set_account_name",
    account
});

// 动作 - 获取某 account id 下的篮子
const GET_BASKETS = (account_id, data = {}) => ({
    type: "get_baskets",
    before: LOADING_SHOW,
    after: LOADING_HIDE,
    async: new Promise(async resolve => {
        data = Object.assign({}, data, { account_id });
        let url = urlSynthesis(getBaskets, account_id);
        let resul = await requestFunc(url, "GET", data);
        resolve(resul);
    })
});

// 动作 - 存储搜索条件
const SAVE_SEARCH_COND = (cond) => ({
    type: "save_search_cond",
    cond
});

// 动作 - 清空baskets内存储信息 
const CLEAR_BASKETS = () => ({ type: "clear_baskets" });


// 动作 - 篮子 - 公共函数
const __BASKET_COMMON = (type, method = "PUT") => 
    (...argv) => ({
        type,
        before: LOADING_SHOW,
        after: LOADING_HIDE,
        async: new Promise(async resolve => {
            let [ account_id, basket_id, data, cb ] = argv;
            let url = urlSynthesis(basketApi, account_id, basket_id);
            let resul = await requestFunc(url, method, data);
            cb && cb(resul);
            resolve(resul);
        })
    });

// 动作 - 篮子 更新字段 - 状态: 启动
const READY_BASKET = __BASKET_COMMON("ready_basket");

// 动作 - 篮子 更新字段 - 重命名
const RENAME_BASKET = __BASKET_COMMON("rename_basket");

// 动作 - 篮子 更新字段 - 动作名: 修改
const UPDATE_BASKET = __BASKET_COMMON("do_opt_basket");

// 动作 - 篮子 删除篮子
const DELETE_BASKET = __BASKET_COMMON("delete_basket", "DELETE");


// 动作 - 不走store 获取篮子下的所有交易/订单
const GET_ORDERS = (account_id, basket_id, data, b) => {
    if (!b) STORE.dispatch(LOADING_SHOW());
    return new Promise(async resolve => {
        let url = urlSynthesis(getOrders, account_id, basket_id);
        let resul = await requestFunc(url, "GET", data);
        resolve(resul);
        if (!b) STORE.dispatch(LOADING_HIDE());
    });
};

// 动作 - 不走store 订单更新字段
const UPDATE_OPTIONS = (account_id, basket_id, order_id, data) => {
    STORE.dispatch(LOADING_SHOW());
    return new Promise(async resolve => {
        let url = urlSynthesis(updateOrder, account_id, basket_id, order_id);
        let resul = await requestFunc(url, "PUT", data);
        resolve(resul);
        STORE.dispatch(LOADING_HIDE());
    });
};

// 动作 - 不走store 修改密码
const CHANGE_PASSWD = async (data) => {
    STORE.dispatch(LOADING_SHOW());
    return new Promise(async resolve => {
        let resul = await requestFunc(changePasswd, "PUT", data);
        resolve(resul);
        STORE.dispatch(LOADING_HIDE());
    });
};

// 动作 - 不走store 刷新验证图片
const REFRESH_VERIFY_IMAGE = async () => {
    STORE.dispatch(LOADING_SHOW());
    return new Promise(async resolve => {
        let resul = await requestFunc(verifyImage, "GET");
        resolve(resul);
        STORE.dispatch(LOADING_HIDE());
    });
};

// 动作 - 不走store 超级管理 - 获取用户列表
const GET_USERS = async () => {
    STORE.dispatch(LOADING_SHOW());
    return new Promise(async resolve => {
        let resul = await requestFunc(superGetUsers, "GET");
        resolve(resul);
        STORE.dispatch(LOADING_HIDE());
    });
};

// 动作 - 不走store 超级管理 - 导出订单
const EXPORT_ORDERS = async (data, fnEnd) => {
    // is_answer
    let { errcode } = await requestFunc(superExportOrders, "GET", { ...data, is_answer: 1 });

    // export
    if (!errcode){
        let url = create_url(superExportOrders);
        let tmp = "";
        for (let [k, v] of Object.entries(data)){
            if (!tmp) tmp += `?${k}=${v}`;
            else tmp += `&${k}=${v}`;
        }
        location.href = url + tmp;
    }
    fnEnd && fnEnd();
};

// 动作 - 不走store 超级管理 - 导出结算价
const EXPORT_SETTLEMENT_PRICE = async (data, fnEnd) => {
    // is_answer
    let { errcode } = await requestFunc(superExportSettlementPrice, "GET", { ...data, is_answer: 1 });

    // export
    if (!errcode){
        let url = create_url(superExportSettlementPrice);
        let tmp = "";
        for (let [k, v] of Object.entries(data)){
            if (!tmp) tmp += `?${k}=${v}`;
            else tmp += `&${k}=${v}`;
        }
        location.href = url + tmp;
    }
    fnEnd && fnEnd();
};


// 动作 - 不走store 下载篮子原始信息
const DOWNLOAD_BASKET = async (account_id, basket_id) => {
    STORE.dispatch(LOADING_SHOW());
    let url = urlSynthesis(downloadBasket, account_id, basket_id);
    let { errcode } = await requestFunc(url, "POST");
    console.log(errcode);
    if (!errcode){
        url = create_url(url);
        location.href = url;
    }
    STORE.dispatch(LOADING_HIDE());
};

// 动作 - 不走store 下载账户仓位信息
const DOWNLOAD_BALANCE = async ({ account_id }, fnEnd) => {
    STORE.dispatch(LOADING_SHOW());
    let url = urlSynthesis(downloadAccountBalance, account_id);
    let { errcode } = await requestFunc(url, "GET", { is_answer: 1 });
    if (!errcode) {
        url = create_url(url);
        location.href = url;
    }
    fnEnd && fnEnd();
    STORE.dispatch(LOADING_HIDE());
};

export {
    GET_ACCOUNTS,
    GET_BASKETS,
    GET_ORDERS,
    READY_BASKET,
    RENAME_BASKET,
    SET_ACCOUNT_NAME,
    LOADING_SHOW,
    LOADING_HIDE,
    DOWNLOAD_BASKET,
    UPDATE_OPTIONS,
    UPDATE_BASKET,
    NAV_STATUS,
    SAVE_SEARCH_COND,
    CLEAR_BASKETS,
    DELETE_BASKET,
    GET_USERS,
    EXPORT_ORDERS,
    EXPORT_SETTLEMENT_PRICE,
    CHANGE_PASSWD,
    REFRESH_VERIFY_IMAGE,
    DOWNLOAD_BALANCE
};

