
// 存储器
import { createStore, combineReducers, applyMiddleware } from 'redux';
import writeOverDispatch from "react-middleware-async";
// import writeOverDispatch from "./redux_async";

import {
    LOADING_REDUCER,
    SET_ACCOUNT,
    SET_ACCOUNTS,
    SET_BASKETS,
    NAV_REDUCER
} from './reducer.js';


/**
 * @desc
 *  accounts: 账户信息
 *      loaded:         是否拉过数据
 *      data:           数据
 *      loading:        loading 显示
 * 
 */

// 兼容server不识别window
const GET_INIT_STATUS = () => {
    try {
        if (window && window.__initialize_status){
            return window.__initialize_status;
        }
    } catch (err){
        // pass
    }
    return {
        loadingState: true,
        accounts: {},
        userInfo: {
            account_display_name: "",
            account_id: 0
        },
        baskets: {
            map: {},
            pager: {},
            cond: {}
        },
        header: {
            current_status: "baskets"
        }
    };
};

// 存储器初始化
const DEFAULT_INITIALIZE = GET_INIT_STATUS();

// console.log(DEFAULT_INITIALIZE);

// 动作处理器 与 存储字段对接,并且初始化 总动作处理器
const REDUCERS = combineReducers({
    loadingState:   LOADING_REDUCER,
    accounts:       SET_ACCOUNTS,
    userInfo:       SET_ACCOUNT,
    baskets:        SET_BASKETS,
    header:         NAV_REDUCER
});


// 建立存储器
const STORE = createStore(REDUCERS, DEFAULT_INITIALIZE, applyMiddleware(writeOverDispatch));

// 服务器端使用
const create_server_store = (initialize_value) => {
    return createStore(REDUCERS, initialize_value, applyMiddleware(writeOverDispatch));
};

export default STORE;

export {
    create_server_store
};

