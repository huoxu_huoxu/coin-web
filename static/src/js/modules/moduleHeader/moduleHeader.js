// header组件
import React from "react";
import { Link, withRouter } from "react-router-dom";  
import { Menu, Icon, Dropdown } from 'antd';
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { header_status } from "../../status/status";
import { user, router_really } from "../../libs/base";
import { destroyToken } from "../../requestApi";
import { loadingShow, loadingHide } from "../moduleLoading/moduleLoading";


class ModuleCommonHeader extends React.Component {

    handleClick  (e){
        let { href } = e.item.props;
        this.setState({
            current: e.key
        });
        return this.props.history.push(`${this.props.match.url}/${href}`);
    }

    switchAccount (e){
        let { account_id } = e.item.props;
        user.account_id = account_id;
        return this.props.history.push(`${this.props.match.url}/accounts/baskets/${account_id}`);
    }

    async quit (){
        loadingShow();
        user.clear();
        await axios.get(`${window.api}${destroyToken}`);
        loadingHide();
        return this.props.history.push(`${router_really()}/login`);
    }

    render() {
        let { user_info: { account_id, account_display_name }, header, accounts } = this.props;

        // 权限
        let agent_user = +user.role === 9 ?  <Menu.Item>
                <Link to={`${router_really()}/main/superAdministrator`}>管理用户</Link>
            </Menu.Item> : "";

        // 用户下拉
        const menu = (
            <Menu className="module_comm_header_menu"> 
                <Menu.Item disabled>
                    {user.nickname}
                </Menu.Item>
                <Menu.Item>
                    <Link to={`${router_really()}/main/accounts/choice/${account_id}`}>选择账户</Link>
                </Menu.Item>
                {agent_user}
                <Menu.Item>
                    <div onClick={this.quit.bind(this)}>
                        <span className={`${router_really()}module_comm_hm_quit`}>退出</span>
                        <Icon type="logout" />
                    </div>
                </Menu.Item>
            </Menu>
        );

        // 账户下拉
        const accounts_menu = (
            <Menu 
                className="module_comm_header_acc_menu" 
                onClick={this.switchAccount.bind(this)}
            >
                {
                    Object.values(accounts).map((account, i) => {
                        return <Menu.Item key={i} account_id={account.id}>
                            <span>{account.display_name}</span>
                        </Menu.Item>
                    })
                }
            </Menu>
        );

        return (
            <div className="module_com_header">

                <aside className="mc_header_icon">
                    QuantDog
                </aside>

                {/* 大导航 */}
                <Menu
                    onClick={this.handleClick.bind(this)}
                    selectedKeys={[header.current_status]}
                    mode="horizontal"
                    className="mc_header_main"
                >
                    <Menu.Item key="baskets" href="baskets">
                        <Icon type="switcher" style={{fontSize: 20}} />篮子列表
                    </Menu.Item>
                    <Menu.Item key="accounts_detail" href={`accounts/detail/${account_id}`}>
                        <Icon type="credit-card" style={{fontSize: 20, position: "relative", top: "2px"}} />账户详情
                    </Menu.Item>
                </Menu>

                {/* 右侧用户 */}
                <section className="mc_header_user">

                    <aside className="hz_cur_po">
                        
                            <div className="mc_header_right">

                                {/* 账户下拉 */}
                                <Dropdown 
                                    overlay={accounts_menu} 
                                    trigger={['click']} 
                                    placement="bottomCenter"
                                >
                                    <li>{account_display_name} <Icon type="down" /></li>
                                </Dropdown>

                                {/* 用户下拉 */}
                                <Dropdown 
                                    overlay={menu} 
                                    trigger={['click']} 
                                    placement="bottomCenter"
                                >
                                    <li className="module_comm_hm_user">
                                        <Icon type="user" style={{"fontSize": "24px"}} />
                                    </li>
                                </Dropdown>
                            </div>
                        
                    </aside>

                </section>
                
            </div>
        );
    }

}


ModuleCommonHeader.propTypes = {
    user_info: PropTypes.any
};



const container = connect(header_status.state)(ModuleCommonHeader);
export default withRouter(container);   
