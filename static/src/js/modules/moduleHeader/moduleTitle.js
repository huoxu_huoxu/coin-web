// header - title 组件
import React from "react";

class ModuleTitle extends React.Component {

    render (){
        let { title, dom } = this.props;
        return (
            <div className="module_com_title">
                {dom}
                <h1>
                    {title}
                </h1>
            </div>
        );

    }

}


export default ModuleTitle;
