// loading组件
import React from "react";
import { Spin } from 'antd';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { LOADING_SHOW, LOADING_HIDE } from "../../status/action"; 
import STORE from "../../status/store";

// 状态
const status = (state, ownProps) => {
    return {
        b: state.loadingState
    };
};

// 方法
const method = (dispatch, ownProps) => {
    return bindActionCreators({
        show: LOADING_SHOW,
        hide: LOADING_HIDE
    }, dispatch);
};

// 包装,对外公开
const { show: loadingShow, hide: loadingHide } = method(STORE.dispatch);

// 支持ssr渲染此组件
class ModuleLoading extends React.Component {
    render (){
        let { b } = this.props;
        return (
            <div className={`module_loading_wrap ${b ? "" : "hz_none"}`}>
                <section className="module_loading_sight">
                    <Spin size="large" tip="Loading..." />
                </section>  
            </div>
        );
    }

}

ModuleLoading.propTypes = {
    b: PropTypes.bool
};

export default connect(status, method)(ModuleLoading);

export {
    loadingShow,
    loadingHide
};
