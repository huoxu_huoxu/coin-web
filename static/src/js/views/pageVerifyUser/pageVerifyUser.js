import React from "react";
import { message } from "antd";
import { router_really } from "../../libs/base";
import { autoLogin } from "../../requestApi";
import { loadingShow, loadingHide } from "../../modules/moduleLoading/moduleLoading";


class VerifyUser extends React.Component {

    async componentDidMount (){
        loadingShow();
        let { data: { errcode, msg } } = await axios(`${window.api}${autoLogin}`, "GET");
        loadingHide();

        if (!errcode) return this.props.history.push(`${router_really()}/main`);

        if(errcode === 1100 || errcode === 1102 || errcode === 5400 || errcode === 1200){
            message.error(msg);
        }

        return this.props.history.push(`${router_really()}/login`);
    }

    render (){
        return (
            <div></div>
        );
    }

}

export default VerifyUser;
