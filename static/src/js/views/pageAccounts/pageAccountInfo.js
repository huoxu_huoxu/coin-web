/**
 * @description
 *  account 账户详情 - 篮子列表
 * 
 */

import React from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table, Row, Col, Button, Input, Select, DatePicker, Modal, message, Icon, Tooltip, Popconfirm } from 'antd';

import { baskets_container } from "../../status/status";
import { getInputs, clearInputs } from "../../libs/dom";
import { DOWNLOAD_BASKET } from "../../status/action";
import { user, dateConversion, tab_sorter, router_really } from "../../libs/base";

const Option = Select.Option;
const RangePicker = DatePicker.RangePicker;

// 操作
const actions = [
    {
        name: "开始",
        type: "danger",
        icon: "play-circle",
        using: [ "wait" ],
        state: "new"
    },
    {
        name: "终止",
        type: "danger",
        icon: "minus-circle",
        using: [ "running" ],
        state: "stop"
    }
];

// 删除按钮可用的状态条件
const delete_btn_available = [ "wait", "finished", "stopped" ];


class AccountInfo extends React.Component {

    state = {
        dates: [],
        status: "",
        account: {},
        // 重命名
        rename: {
            visible: false,
            id: null,
            basket_name: ""
        },
        // 记录处于运行中的个数
        i_running: 0,
        // 显示列表
        list: [],
        // 排序
        sortedInfo: {}
    };

    // 不允许无反馈前重复通讯
    b_ajax = false;

    handleChange (pagination, filters, sorter){

        // 排序
        let { sortedInfo } = this.state;

        // 区分触发函数的原因
        if (sorter.columnKey){
            if (sortedInfo.columnKey !== sorter.columnKey || sortedInfo.order !== sorter.order){
                let n = _.cloneDeep(this.state.list);
                n = tab_sorter(n, sorter.columnKey, sorter.order);

                this.setState({
                    list: n,
                    sortedInfo: sorter
                });
            }
            return ;
        }
        
        // 分页 - 拉取数据
        let { baskets: { pager, cond }, get_data } = this.props;
        if (pager.current !== pagination.current){
            get_data(this.state.account.id, Object.assign({}, cond, { page_index: pagination.current }));
        }

        this.setState({
            sortedInfo: {}
        });
    }

    // 篮子 就绪, 终止, 删除
    confirmBasketStatus (id, state){
        let { update_do_opt, delete_basket } = this.props;
        let data = {}, action = update_do_opt, cb;
        switch (state){
            case "stop": 
                data = { do_opt: "stop" }; 
                break;
            case "new": 
                data = { status: "new" };
                break;
            case "del": 
                action = delete_basket;
                cb = () => {
                    let n = _.cloneDeep(this.state.account);
                    n.count--;
                    this.setState({
                        account: n
                    });
                };
                break;
            default: message.warn("存在异常"); return;
        }
       
        this.ajaxPacking(() => {
            return new Promise(resolve => {
                action.bind(
                    this, 
                    this.state.account["id"], 
                    id,
                    data,
                    () => {
                        cb && cb();
                        resolve();
                    }
                )();
            });
        });
    }

    // 统一抽离ajaxing表现形式
    async ajaxPacking (ajaxing){
        if (this.b_ajax) return ;
        this.b_ajax = true;
        await ajaxing();
        this.b_ajax = false;
    }

    // 下载
    download (id){
        DOWNLOAD_BASKET(this.state.account["id"], id);
    }
    
    // 重命名
    rename (id, record){
        this.setState({
            rename: {
                visible: true,
                basket_name: record["filename"],
                id,
            }
        });
        let filename_dom = ReactDOM.findDOMNode(this.filename_input.current);
        filename_dom && (filename_dom.value = record['filename']);
    }
    rename_OK (){

        // 通信状态不允许重复发送
        if (this.b_ajax) return message.warn("已在修改, 请稍候");

        // 不允许为空、过长
        let new_filename = document.querySelector(".page_acc_file_name").value;
        if (!new_filename) return message.warn("请输入新的篮子名称");
        if (new_filename.length > 100) return message.warn("篮子名称过长, 请精简");

        // 提交
        let { rename_basket } = this.props;
        rename_basket(this.state.account["id"], this.state.rename.id, { new_filename }, (resul) => {
            let { errcode } = resul;

            this.b_ajax = false;
            // 成功后收起
            if (!errcode) {
                this.rename_CANCEL();
                return message.success("成功");
            }
        });

        this.b_ajax = true;
    }
    rename_CANCEL (){
        this.setState({
            rename: {
                visible: false
            }
        });
    }

    componentWillMount (){

        let { match, accounts, set_account, get_data, nav_status, baskets } = this.props;
        let { account_id } = match.params;
        let item = accounts[account_id];

        // 初始化state
        let tmp = { account: _.cloneDeep(item) };
        baskets["cond"]["status"] && (tmp["status"] = baskets["cond"]["status"]);
        baskets["cond"]["tmp_dates"] && (tmp["dates"] = baskets["cond"]["tmp_dates"])
        this.setState(tmp);

        // 初始化dom
        this.filename_input = React.createRef();
        
        // 记录account
        set_account(item);

        // 拉取数据
        get_data(item.id, Object.assign({}, baskets["cond"], { page_index: baskets["pager"]["current"] || 1 }));

        // 记录account id
        user.set_localStorage("default_account_id", item.id);

        // 切换nav
        nav_status("baskets");

    }

    componentWillReceiveProps (nextProps){

        // 路由发生了变化
        if (this.props.match.url !== nextProps.match.url){
            let { clear_baskets } = this.props;
            // 清空store锁定搜索状态
            clear_baskets();
            // 清空组件state及value
            clearInputs();
            this.setState({
                dates: [],
                status: ""
            });
            // 修改js执行序列
            return setTimeout(this.componentWillMount.bind(this), 0);

        } else {

            let { baskets } = nextProps;

            let list = [], i_running = 0, tmp = {}, b_search = false;
            for (let v of Object.values(baskets.map)) {
                list.push(v);
                if (v.status === "running") i_running++;
            }
            list.reverse();

            // 保证运行中的篮子数只有在无条件搜索时才会重算更新
            for (let v of Object.values(getInputs())){
                if (v){
                    b_search = true;
                    break;
                }
            }
            if (!this.state.status && !b_search && !this.state.dates.length){
                tmp["i_running"] = i_running;

                // 如果以非搜索状态下拉取了篮子列表, 重置篮子总数
                tmp["account"] = _.cloneDeep(this.state.account);
                tmp["account"]["count"] = baskets.pager.total;
            }

            tmp['list'] = list;
            return this.setState(tmp);

        }
        
    }

    addBasket (){
        return this.props.history.push(`${router_really()}/main/accounts/addBasket/${this.state.account.id}`);
    }

    searching (){
        let { get_data, save_search_cond } = this.props;
        let { account, dates, status } = this.state;
        let tmp = Object.assign({}, getInputs(), { status });
        if (dates.length){
            tmp["started_time"] = dates[0].toString();

            // 只有结束状态下才有结束时间, 其他只按开始时间算, 过滤结束时间条件
            if (status === "finished"){
                tmp["ended_time"] = dates[1].toString();
            }

            // 暂存, 用于展示
            tmp["tmp_dates"] = dates;
        }


        save_search_cond(tmp);
        get_data(account["id"], tmp);

    }

    componentWillUnmount (){
        if (!/\/accounts\/\d+\/\d+/.test(location.pathname)){
            let { clear_baskets } = this.props;
            clear_baskets();
        }
    }

    commonSearching (key, e){
        let tmp = {};
        tmp[key] = e;
        this.setState(tmp);
        
        // 取消时间限制时, 不发起请求
        if (key === "dates" && !e.length) return ;

        // 事件轮询机制, 让微任务列队先执行
        setTimeout(() => {
            this.searching();
        }, 0);
    } 

    inputs = [
        {
            name: "篮子名",
            placehold: "请输入篮子名称",
            v_name: "filename",
            offset: 0
        },
        {
            name: "包含币种",
            placehold: "请输入币种",
            v_name: "coin_name",
            offset: 1
        }
    ];

    render (){
        let { baskets, sortedInfo } = this.props;
        sortedInfo = sortedInfo || {};

        let columns = [
            {
                title: '篮子名',
                dataIndex: 'filename',
                key: 'filename',
                className: "page_baskets_name",
                width: 330,
                render: (text, record) => {
                    let arr = this.props.match.url.split("/");
                    arr.splice(arr.length-2, 1);
                    let s = arr.join("/");
                    let url = `${s}/${record.id}`;

                    // 删除按钮
                    let del;
                    let b = delete_btn_available.filter(v => v === record['status']).length;
                    let s_class = "iconfont icon-close-circle page_basket_name_i delete";
                    if (b){
                        del = <Popconfirm 
                                placement="top" 
                                title={`是否确定删除 ${record.filename} ?`} 
                                onConfirm={this.confirmBasketStatus.bind(this, record.id, "del")}
                                okText="Yes" 
                                cancelText="No"
                            >
                            <i className={s_class}></i>
                        </Popconfirm>;
                    } else {
                        del = <i className={s_class} onClick={() => {message.warn("不能删除未结束的篮子");}} />
                    }

                    return <div className="page_basket_link_wp">
                        <nav className="page_basket_link">
                            <Link title={text} to={url} >{text}</Link>
                        </nav>
                        <aside className="page_basket_btns">
                            <Tooltip title="重命名" mouseLeaveDelay="0.5" mouseEnterDelay="0.5">
                                <i className="iconfont icon-edit page_basket_name_i" onClick={this.rename.bind(this, record.id, record)}></i>
                            </Tooltip>
                            <Tooltip title="删除" mouseLeaveDelay="0.5" mouseEnterDelay="0.5">
                                {del}
                            </Tooltip>
                            <Tooltip title="下载" mouseLeaveDelay="0.5" mouseEnterDelay="0.5">
                                <i className="iconfont icon-download page_basket_name_i download" onClick={this.download.bind(this, record.id)}></i>
                            </Tooltip>
                        </aside>
                    </div>;
                },
                sorter: true,
                sortOrder: sortedInfo.columnKey === "filename" && sortedInfo.order
            }, {
                title: '状态',
                dataIndex: 'status',
                key: 'status',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "status" && sortedInfo.order
            }, {
                title: '订单',
                dataIndex: 'order_count',
                key: 'order_count',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "order_count" && sortedInfo.order
            }, {
                title: '开始日期',
                dataIndex: 'started_at',
                key: 'started_at',
                render: (text) => text && dateConversion(text),
                sorter: true,
                sortOrder: sortedInfo.columnKey === "started_at" && sortedInfo.order
            }, {
                title: '结束日期',
                dataIndex: 'ended_at',
                key: 'ended_at',
                render: (text) => text && dateConversion(text),
                sorter: true,
                sortOrder: sortedInfo.columnKey === "ended_at" && sortedInfo.order
            }, {
                title: '操作',
                dataIndex: 'id',
                key: 'id',
                className: "p_a_td_maxwidth",
                width: 200,
                render: (id, record) => {
                    return <Row>
                        {
                            actions.map((v, i) => {
                                // 常规 btn
                                let btn = <Button 
                                        type={v.type}
                                        icon={v.icon}
                                    >
                                        {v.name}
                                    </Button>;
                                // 不可用btn
                                let a_status = v.using.filter(v => v === record['status']);
                                let b_disabled = a_status.length ? false : true;

                                if (b_disabled){
                                    btn = <Button
                                        type={v.type} 
                                        icon={v.icon}
                                        disabled
                                    >
                                        {v.name}
                                    </Button>;
                                }
                                return <Col span="12" key={i}>
                                    <Popconfirm 
                                        placement="top" 
                                        title={`是否确定${v.name} ${record.filename} ?`} 
                                        onConfirm={this.confirmBasketStatus.bind(this, id, v.state)}
                                        okText="Yes" 
                                        cancelText="No"
                                    >
                                        {btn}
                                    </Popconfirm>
                                </Col>
                            })
                        }
                    </Row>
                }
            }
        ];

        // ... env dev
        if (window.debug){
            columns.unshift({
                title: "id",
                dataIndex: "id",
                key: "id"
            });
        }
        
        return (
            <div>

                <section className="page_account_inputs2">

                    <Row align="middle">
                        {
                            this.inputs.map((v, i) => {
                                return <div key={i}>
                                    <Col span={2} offset={v.offset} className="page_account_span">
                                        <span>{v.name}:</span>
                                    </Col>
                                    <Col span={4}>
                                        <Input 
                                            onPressEnter={this.commonSearching.bind(this, "")} 
                                            name={v.v_name} 
                                            placeholder={v.placehold}
                                            defaultValue={baskets["cond"][v.v_name]}
                                        />
                                    </Col>
                                </div>;
                            })
                        }
                    </Row>

                    <Row align="middle" style={{"marginTop": "10px"}}>
                        <Col span={2} className="page_account_span">
                            <span>状态:</span>
                        </Col>
                        <Col span={4}>
                            <Select 
                                onSelect={this.commonSearching.bind(this, "status")} 
                                defaultValue={this.state.status}
                                value={this.state.status}
                                style={{ width: 200 }} 
                            
                            >
                                <Option value="">全部</Option>
                                <Option value="wait">wait</Option>
                                <Option value="new">new</Option>
                                <Option value="running">running</Option>
                                <Option value="stopped">stopped</Option>
                                <Option value="pending">pending</Option>
                                <Option value="finished">finished</Option>
                            </Select>
                        </Col>
                        <Col span={2} offset={1} className="page_account_span">
                            <span>起止时间:</span>
                        </Col>
                        <Col span={6}>
                            <RangePicker 
                                onChange={this.commonSearching.bind(this, "dates")}
                                defaultValue={this.state.dates}
                                value={this.state.dates}
                            />
                        </Col>
                    </Row>

                </section>

                <Row style={{"margin": "0px 0 20px", "padding": "0 8px"}}>
                    <Col span={2} offset={22} style={{textAlign: "right"}} >
                        <Button onClick={this.addBasket.bind(this)} icon="plus" size="large">添加篮子</Button>
                    </Col>
                </Row>

                <Table
                    columns={columns}
                    dataSource={this.state.list}
                    bordered
                    pagination={false}
                    className="page_sorted_table"
                    footer={() => (`目前在运行 ${this.state.i_running} 个篮子, 本账号共${this.state.account.count}个篮子`)}
                    onChange={this.handleChange.bind(this)}
                    pagination={baskets.pager}
                />
                
                <Modal
                    title="重命名"
                    visible={this.state.rename.visible}
                    onOk={this.rename_OK.bind(this)}
                    onCancel={this.rename_CANCEL.bind(this)}
                    okText="提交"
                    cancelText="关闭"
                    maskClosable={false}
                >
                    <Input 
                        ref={this.filename_input} 
                        size="large"
                        defaultValue={this.state.rename.basket_name}
                        className="page_acc_file_name" 
                        placeholder="请输入新的篮子名称" 
                    />
                    
                </Modal>

            </div>
        );
    }

}

AccountInfo.propTypes = {
    match: PropTypes.any
};



const container = connect(baskets_container.state, baskets_container.props)(AccountInfo);

export default container;
