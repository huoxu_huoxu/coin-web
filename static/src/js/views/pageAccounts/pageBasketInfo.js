/**
 * @description
 *  basket 篮子详情 - 交易列表
 * 
 */

import React from "react";
import PropTypes from "prop-types";
import { Table, Progress, Row, Col, Button, Input, Select, DatePicker, Modal, message, Icon, Popconfirm } from 'antd';

import { GET_ORDERS, UPDATE_OPTIONS, UPDATE_BASKET } from "../../status/action";
import ModuleTitle from "../../modules/moduleHeader/moduleTitle";
import STORE from "../../status/store";
import { getInputs, setInputs } from "../../libs/dom";
import { test, tab_sorter, toE } from "../../libs/base";
import { loadingShow, loadingHide } from "../../modules/moduleLoading/moduleLoading";

const Option = Select.Option;
// const RangePicker = DatePicker.RangePicker;

// 操作
const actions = [
    {
        name: "暂停",
        onClick: "update_do_options",
        do_opt: "pause"
    }, {
        name: "恢复",
        onClick: "update_do_options",
        do_opt: "resume"
    }, {
        name: "设置参数",
        onClick: "showModal"
    }
];

// 拉取数据
const get_data = async (account_id, basket_id, search_cond, b, self) => {
    let { errcode, data } = await GET_ORDERS(account_id, basket_id, search_cond, b);
    if (errcode !== 0) return ;

    // 修正参数
    let { list, basket } = data;
    let default_process = {
        status: "new",
        trade: 0,
        notrade: "---",
        pct: 0,
        current_algo_param: "",
        average_price: ""
    };
    for (let [i, item] of Object.entries(list)) {

        // 如果没有相关数据, 生成默认的
        let v_process = item.process;
        if (!v_process) {
            v_process = _.cloneDeep(default_process);
            v_process.notrade = item.qty;
        }
        delete item['process'];
        item.key = i;

        // 保证小数点后最多 8 位
        let [ , decimal ] = v_process['trade'].toString().split(".");
        if (decimal && decimal.length > 8) v_process['trade'] = v_process['trade'].toFixed(8);

        // 币种合并显示
        item["coin"] = item.base_asset;
        if (item.quote_asset){
            item["coin"] += `/${item.quote_asset}`;
        }
        

        // 小数点限制
        item["pct"] = (+item.pct).toFixed(0);

        // 实际当前运行的算法参数
        if (v_process["current_algo_param"] && v_process["current_algo_param"] !== "{}"){
            v_process["current_algo_param"] = JSON.parse( v_process["current_algo_param"] );

            // 用户设置参数替换算法参数
            let a_tmp = [];
            for (let [k,v] of Object.entries(v_process["current_algo_param"])){
                a_tmp.push(`${k}=${v}`);
            }

            // 有值时覆盖, 无值时显示用户上传csv的值
            item.algo_param = a_tmp.join("|");
        } else {
            v_process["current_algo_param"] = {};
        }

        // 均价 有效位数 8 位
        v_process["average_price"] && (v_process["average_price"] = +(v_process["average_price"].toFixed(8)));

        delete v_process["id"];
        delete item["base_asset"];
        delete item["quote_asset"];

        Object.assign(item, v_process);
    }

    // 实时获取数据, 并且不修改排序规则
    if (b){
        let { sortedInfo } = self.state;
        if (sortedInfo.columnKey){
            list = tab_sorter(list, sortedInfo.columnKey, sortedInfo.order);
        }
    }

    self.setState({
        list,
        basket
    });

    console.log(list);
};

// 算法相关
const algo_config = {
    EPOV: {
        user_pov: {
            placeholder: "pov"
        },
        user_spread_threshold: {
            placeholder: "spread_threshold"
        },
        user_limit_price: {
            placeholder: "limit_price"
        }
    },
    POV: {
        user_pov: {
            placeholder: "pov"
        },
        user_spread_threshold: {
            placeholder: "spread_threshold"
        },
        user_limit_price: {
            placeholder: "limit_price"
        }
    },
    PI: {
        user_pov: {
            placeholder: "pov"
        },
        user_spread_threshold: {
            placeholder: "spread_threshold"
        },
        user_limit_price: {
            placeholder: "limit_price"
        }
    },
    TWAP: {},
    Fisher: {},
};


class BasketInfo extends React.Component {

    state = {
        list: [],
        sortedInfo: {},
        account: {},
        account_id: null,
        basket_id: null,
        basket: {},
        status: "",
        dates: [],
        set_options_visible: false,
        b_algo_param: false,
        s_setting: false,
        order: null
    };

    b_ajax=false;

    handleChange (pagination, filters, sorter){

        // 排序
        let { sortedInfo } = this.state;

        // 区分触发函数的原因
        if (sorter.columnKey){
            if (sortedInfo.columnKey !== sorter.columnKey || sortedInfo.order !== sorter.order){
                let n = _.cloneDeep(this.state.list);
                n = tab_sorter(n, sorter.columnKey, sorter.order);

                this.setState({
                    list: n,
                    sortedInfo: sorter
                });
            }
            return ;
        }

        this.setState({
            sortedInfo: {}
        });
    }

    componentWillMount() {

        let { params: { account_id, basket_id } } = this.props.match;
        basket_id = +basket_id

        let tmp = STORE.getState(), basket;
        let account = tmp["accounts"][account_id];
        basket = tmp.baskets['map'][basket_id];

        this.setState({
            account,
            account_id,
            basket_id,
            basket
        });

        // 定时拉取
        this.timer = setInterval(() => {
            this.searching(true);
        }, 4000);

        get_data(account_id, basket_id, {}, false, this);
        
    }

    componentWillUnmount (){
        clearInterval(this.timer);
        this.timer = null;
    }

    algoOpen (){
        let b = this.state.b_algo_param;
        this.setState({
            b_algo_param: !b
        });
    }

    searching(b = false) {

        let { account_id, basket_id, dates, status } = this.state;
        let tmp = Object.assign({}, getInputs(), { status });
        if (dates.length) {
            tmp["started_time"] = dates[0].toString();
            tmp["ended_time"] = dates[1].toString();
        }

        get_data(account_id, basket_id, tmp, b, this);
    }

    inputs = [
        {
            name: "订单币种",
            placehold: "请输入币种",
            v_name: "coin_name",
            offset: 0
        }
    ];


    async update_do_options (item, action){
        let { account_id, basket_id } = this.state;
        let { errcode } = await UPDATE_OPTIONS(account_id, basket_id, item['id'], { do_opt: action });
        if (!errcode){
            let list = _.cloneDeep(this.state.list);
            list.map(v => {
                if (v.id === item.id){
                    v.do_opt = action;
                    v.status = "pending";
                }
                return v;
            });
            this.setState({
                list
            });
        }
    }

    // 设置用户自定义系数
    async handleOk() {

        if (this.b_ajax) return ;

        // 需要优化, 动态获取
        let inputs= getInputs(".page_acc_file_name");
        let { qty } = inputs;
        if (qty && +qty < this.state.order.trade) return message.warn("总量值不能小于已完成的值");

        // 动态合成 并且 转number
        let tmp = {};
        for (let [ k , v ] of Object.entries(inputs)){
            if (k === "qty" && v) {
                tmp[k] = +v;
                if (!test.isNumber(tmp[k])) return message.warn("总量 需要填写数字");
                continue;
            }
            if (v){
                if (!tmp.user_algo_param) tmp.user_algo_param = {};
                tmp.user_algo_param[k] = +v;

                // 动态部分的字段不进行校验, 需要透传给灰灰校验, 暂时我这里硬编码校验
                if (!test.isNumber(tmp.user_algo_param[k])) {
                    return message.warn(`${algo_config[this.state.order.algo_name][k]["placeholder"]} 需要填写数字`);
                }
                
            }
        }

        // 非空验证
        if (JSON.stringify(tmp) === "{}") return message.warn("请至少填写一项");
        console.log(tmp);

        // sumbit
        let { account_id, basket_id } = this.state;
        this.b_ajax = true;
        let { errcode } = await UPDATE_OPTIONS(account_id, basket_id, this.state.order.id, tmp);
        this.b_ajax = false;
        if (!errcode){
            this.setState({
                set_options_visible: false
            });
            return message.success("success");
        }
    }

    showModal (item){
        let tmp = {};
        if (item){

            if (item.algo_name === "TWAP") return message.warning("TWAP 算法，暂时不可设置参数");

            tmp['set_options_visible'] = true;
            tmp['order'] = item;

            // 显示当前值 visible=true后ref绑定, 执行顺序慢于findDOMNode, 将其延迟到下一轮事件轮询
            setTimeout(() => {
                setInputs(".page_acc_file_name", Object.assign({}, item.current_algo_param, { qty: item.qty }));
            }, 0);

        } else {
            tmp['set_options_visible'] = false;
            tmp['order'] = null;
        }

        this.setState(tmp);
    }

    commonSearching (key, e){
        let tmp = {};
        tmp[key] = e;
        this.setState(tmp);
        
        // 事件轮询机制, 让微任务列队先执行
        setTimeout(() => {
            this.searching();
        }, 0);
    } 

    setting (){
        this.setState({
            b_setting: !this.state.b_setting
        });
    }

    confirmBasketStatus (state){
        let data, status;
        switch (state){
            case "stop": 
                data = { do_opt: "stop" }; 
                status = { status: "pending" };  
                break;
            case "new": 
                data = { status: "new" };
                status = { status: "new" };
                break;
            default: message.warn("存在异常"); return;
        }

        this.ajaxPacking(
            UPDATE_BASKET.bind(
                this, 
                this.state.account_id, 
                this.state.basket.id, 
                data,
                (resul) => {
                    let { errcode } = resul;
                    if (!errcode) {
                        this.setState({
                            basket: Object.assign({}, this.state.basket, status)
                        })
                        return message.success("成功");
                    }
                }
            )
        );
    }

    async ajaxPacking (ajaxing){
        if (this.b_ajax) return ;
        this.b_ajax = true;
        loadingShow();
        await ajaxing().async;
        loadingHide();
        this.b_ajax = false;
    }

    render() {

        let { sortedInfo } = this.props;
        sortedInfo = sortedInfo || {};

        let dom = <Button size="large" onClick={window.history.back.bind(window.history)}>返回</Button>;


        // coin transaction
        let coinTrans = [
            { 
                title: '整体进度',
                dataIndex: 'pct',
                key: 'pct',
                width: 150,
                render: (text, record) => {
                    let pro = <Progress percent={record.pct} />;
                    if (record.status === "finished"){
                        pro = <Progress percent={record.pct} strokeColor="#00a854" />;
                    }
                    return pro;
                },
                sorter: true,
                sortOrder: sortedInfo.columnKey === "pct" && sortedInfo.order
            },
            {
                title: '已完成',
                dataIndex: 'trade',
                key: 'trade',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "trade" && sortedInfo.order
            }, {
                title: '总量',
                dataIndex: 'qty',
                key: 'qty',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "qty" && sortedInfo.order
            },
            {
                title: '余额',
                dataIndex: 'balance',
                key: 'balance',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "balance" && sortedInfo.order
            },
            {
                title: '均价',
                dataIndex: 'average_price',
                key: 'average_price',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "average_price" && sortedInfo.order
            }
        ];

        // futures contract
        let futuresCont = [
            {
                title: '现在仓位',
                dataIndex: 'trade',
                key: 'trade',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "trade" && sortedInfo.order
            }, {
                title: '目标仓位',
                dataIndex: 'qty',
                key: 'qty',
                width: 100,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "qty" && sortedInfo.order
            }
        ]

        // exchange
        let exchangeColumns, algoParamsWith;
        switch (this.state.account.exchange){
            case "Bitmex": {
                exchangeColumns = futuresCont;
                algoParamsWith = {
                    "maxWidth": "200px",
                    "width": "200px"
                };
                break;
            }
            default: {
                exchangeColumns = coinTrans;
                algoParamsWith = {
                    "maxWidth": "100px",
                    "width": "100px"
                };
            }
        }

        // common columns
        let columns = [
            {
                title: '货币',
                dataIndex: 'coin',
                key: 'coin',
                width: 120,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "coin" && sortedInfo.order
            }, {
                title: '买/卖',
                dataIndex: 'direction',
                key: 'direction',
                width: 70,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "direction" && sortedInfo.order
            }, {
                title: '状态',
                dataIndex: 'status',
                key: 'status',
                width: 90,
                render: (text, record) => {
                    let dom = text;
                    if (record.error_msg){
                        let { status, error_msg: msg } =  record;
                        if (status === "error"){
                            dom = <div onClick={() => {message.error(msg);}}><span>{text}</span><Icon type="exclamation-circle" className="page_account_error" /></div>;
                        }
                        if (status === "warning"){
                            dom = <div onClick={() => {message.warn(msg);}}><span>{text}</span><Icon type="exclamation-circle" className="page_account_warn" /></div>;
                        }
                    }
                    return dom;
                },
                sorter: true,
                sortOrder: sortedInfo.columnKey === "status" && sortedInfo.order
            },
            ...exchangeColumns,
            {
                title: '操作',
                dataIndex: 'action',
                key: 'action',
                width: this.state.b_setting ? 260 : 80,
                align: "center",
                className: "page_acc_setting_wrap",
                render: (text, record) => {
                    return <Row style={{"textAlign": "center"}} className="page_acc_setting">
                        {
                            do {
                                if (this.state.b_setting){
                                    actions.map((v, i) => {
                                        let btn;
                                        if (v.do_opt){
                                            let disabled = true;
                                            if (record['status'] === "running" && v.do_opt === "pause") disabled = false;
            
                                            if (record['status'] === "paused" && v.do_opt === "resume") disabled = false;
                                            
                                            if (record["error_msg"] && v.do_opt === "pause" && record['status'] !== "pending") disabled = false;
            
                                            btn = <Button 
                                                onClick={this[v.onClick]['bind'](this, record, v.do_opt)}
                                                disabled={disabled}
                                                size="small"
                                            >  
                                                {v.name}
                                            </Button>;
                                        } else {
                                            btn = <Button 
                                                onClick={this[v.onClick]['bind'](this, record)}
                                                size="small"
                                            >  
                                                {v.name}
                                            </Button>;
                                        }
                                        return <Col span="6" key={i}>
                                            {btn}
                                        </Col>;
                                    })
                                }
                            }
                        }
                        <Col span="6">
                            <Icon 
                                type="setting" 
                                onClick={this.setting.bind(this)} 
                                style={{"fontSize": "16px"}} 
                                className="hz_cur_po" 
                            />
                        </Col>
                    </Row>
                }
            },
            {
                title: "算法",
                dataIndex: "algo_name",
                key: "algo_name",
                width: 80,
                sorter: true,
                sortOrder: sortedInfo.columnKey === "algo_name" && sortedInfo.order
            },
            {
                title: '参数',
                dataIndex: 'algo_param',
                key: 'algo_param',
                className: "page_order_list_algo",
                render: (text) => {
                    if (!this.state.b_algo_param){
                        return <div onClick={this.algoOpen.bind(this)} style={algoParamsWith} className="p_a_td_algo_width">
                            <a href="javascript:void(0);">{text}</a>
                        </div>;
                    }
                    return <div onClick={this.algoOpen.bind(this)} style={{"whiteSpace": "nowrap"}}>
                        <a href="javascript:void(0);">{text}</a>
                    </div>;
                }
            }
        ];

        // ... env dev
        if (window.debug){
            columns.unshift({
                title: "id",
                dataIndex: "id",
                key: "id",
                width: 60
            });
        }

        // 解决, ssr 只拉了
        let { basket } = this.state;
        let basketName = "";
        let btnFinallyBasket = <Button type="danger" icon="minus-circle" disabled>终止</Button>;
        let btnStartBasket = <Button type="danger" icon="minus-circle" disabled>开始</Button>;
        if (basket){
            basketName =  basket.filename;
            if (basket.status === "running"){
                btnFinallyBasket = <Button type="danger" icon="minus-circle">终止</Button>;
            }
            if (basket.status === "wait"){
                btnStartBasket = <Button type="danger" icon="play-circle">开始</Button>;
            }
        }

        return (
            <div>

                <ModuleTitle title={`篮子 ${basketName} 订单列表`} dom={dom} />

                <hr className="page_account_hr" />

                <section className="page_account_inputs">

                    <Row align="middle">
                        {
                            this.inputs.map((v, i) => {
                                return <div key={i}>
                                    <Col span={2} offset={v.offset} className="page_account_span">
                                        <span>{v.name}:</span>
                                    </Col>
                                    <Col span={4}>
                                        <Input 
                                            name={v.v_name} 
                                            placeholder={v.placehold} 
                                            onPressEnter={this.commonSearching.bind(this, "")}    
                                        />
                                    </Col>
                                </div>;
                            })
                        }

                        <Col span={1} offset={1} className="page_account_span">
                            <span>状态:</span>
                        </Col>
                        <Col span={4}>
                            <Select 
                                onSelect={this.commonSearching.bind(this, "status")} 
                                defaultValue={this.state.status} 
                                style={{ width: 200 }} 
                            >
                                <Option value="">全部</Option>
                                <Option value="new">new</Option>
                                <Option value="running">running</Option>
                                <Option value="stopped">stopped</Option>
                                <Option value="paused">paused</Option>
                                <Option value="warning">warning</Option>
                                <Option value="error">error</Option>
                                <Option value="pending">pending</Option>
                                <Option value="finished">finished</Option>
                            </Select>
                        </Col>
                        
                        <Col span={2} offset={8} style={{"textAlign": "right"}}>
                            <Popconfirm placement="top" title={`是否确定开始 ${basketName} ?`} onConfirm={this.confirmBasketStatus.bind(this, "new")} okText="Yes" cancelText="No">
                                { btnStartBasket }
                            </Popconfirm>
                        </Col>
                        <Col span={2} style={{"textAlign": "right"}}>
                            <Popconfirm placement="top" title={`是否确定终止 ${basketName} ?`} onConfirm={this.confirmBasketStatus.bind(this, "stop")} okText="Yes" cancelText="No">
                                { btnFinallyBasket }
                            </Popconfirm>
                        </Col>

                    </Row>

                </section>


                {/* 表格 */}
                <div className="page_orders_contain">
                    <Table
                        columns={columns}
                        dataSource={this.state.list}
                        bordered
                        onChange={this.handleChange.bind(this)}
                        pagination={false}
                        footer={() => `本篮子共 ${this.state.list.length} 个订单`}
                        scroll={{y: 600, x: this.state.b_setting ? 1380 : 1180}}
                        className="page_sorted_table"
                        size="middle"
                    />
                </div>

                
                {/* 输入遮罩层 */}
                <Modal
                    title="设置参数"
                    visible={this.state.set_options_visible}
                    onOk={this.handleOk.bind(this)}
                    onCancel={this.showModal.bind(this, false)}
                    okText="提交"
                    cancelText="关闭"
                    maskClosable={false}
                >

                    {
                        do {
                            if (this.state.order && this.state.order["algo_name"] in algo_config){
                                Object.entries(algo_config[this.state.order["algo_name"]]).map(([k, v], i) => {
                                    return <Input 
                                        key={i}
                                        name={k}
                                        curr={v.placeholder}
                                        size="large"
                                        className="page_acc_file_name" 
                                        placeholder={`请输入 ${v.placeholder}`}
                                    />
                                })
                            }
                        }
                    }
                    <Input 
                        name="qty"
                        size="large"
                        className="page_acc_file_name" 
                        placeholder="请输入 总量"
                    />
                </Modal>
                
            </div>
        );
    }

}

BasketInfo.propTypes = {
    match: PropTypes.any
};


export default BasketInfo;