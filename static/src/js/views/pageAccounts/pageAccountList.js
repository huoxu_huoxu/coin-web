/**
 * @description
 *  account 账户列表
 * 
 */


import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table } from 'antd';

import ModuleTitle from "../../modules/moduleHeader/moduleTitle";
import { accounts_container } from "../../status/status";
import { user } from "../../libs/base";


class AccountList extends React.Component {

    columns = [
        {
            title: '账户',
            dataIndex: 'display_name',
            key: 'display_name',
            render: (text, record) => {
                let arr = this.props.match.url.split("/");
                arr.length = (arr.length - 2);
                arr.push("baskets", record.id);
                let url = `${arr.join("/")}`;

                // 变更内存中的当前 account_id 记录
                return <Link onClick={()=>{ user.account_id = record.id;}} to={url} >{text}</Link>;
            },
        }, {
            title: '正在运行的篮子',
            dataIndex: 'running_count',
            key: 'running_count'
        }, {
            title: '交易所',
            dataIndex: 'exchange',
            key: 'exchange'
        }
    ];
    

    componentWillMount() {
        let { get_data, nav_status } = this.props;

        nav_status("accounts");
        get_data();
    }

    render() {
        let { accounts } = this.props;
        let tmp = [];
        for (let v of Object.values(accounts)){
            tmp.push(v);
        }

        return (
            <div>

                <ModuleTitle title="账户列表" />
                <hr className="page_account_hr" />

                <section className="pa_tb_detail">
                    <Table
                        columns={this.columns}
                        dataSource={tmp}
                        bordered
                        pagination={false}
                    />
                </section>

            </div>
        );
    }

}

AccountList.propTypes = {
    get_data: PropTypes.func
};

const Container = connect(accounts_container.state, accounts_container.props)(AccountList);
export default Container;
