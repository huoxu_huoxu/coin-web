/**
 * @description
 *  account 路由体系
 * 
 */


import React from "react";
import { Route, Switch } from "react-router-dom";
import PropTypes from "prop-types"; 
import { message } from 'antd';

import AccountList from "./pageAccountList";
import AccountInfo from "./pageAccountInfo";
import AddBasket from "./pageAddBasket";
import BasketInfo from "./pageBasketInfo";
import AccountDetail from "./pageAccountDetail";

import STORE from "../../status/store";
import { user } from "../../libs/base";
import diyWebWorker from "../../libs/worker";
import { loadingShow, loadingHide } from "../../modules/moduleLoading/moduleLoading";

import { GET_ACCOUNTS } from "../../status/action"

class Accounts extends React.Component {

    async componentWillMount (){

        if (!/\/main\/accounts$/.test(location.pathname)) return console.log("其他页面刷新切入");

        let { match } = this.props;

        // 第一次进入拉基础数据
        loadingShow();
        let ret = await GET_ACCOUNTS().async;
        let { errcode, data } = ret;
        loadingHide();

        if (!errcode){
            let { list: accounts } = data;
            
            STORE.dispatch({
                type: "get_accounts",
                async: ret
            });

            let account_id = +user.account_id;
            let account;

            if (account_id) ([ account ] = accounts.filter(v => v.id === account_id));
            if (!account_id) account = accounts[0];
            if (!account)  account = accounts[0];

            // 开始建立, ...
            {
                if (window.debug){
                    diyWebWorker.start();
                }
            }

            if (account){
                STORE.dispatch({
                    type: "set_account_name",
                    account
                });

                return this.props.history.push(`${match.url}/baskets/${account.id}`);
            }

            // 提示用户创建账号
            message.error("您还没有账户, 请联系管理人员, 申请账户");
            
        }
        
    }

    componentWillReceiveProps (){
        if (/\/main\/accounts$/.test(location.pathname)){
            // 是否是第一次进入, 如果不是第一次进入, 此路由作为承上启下作用, 回到在上一级
            let store = STORE.getState();
            if (store.userInfo.account_id) {
                window.history.back();
            }
        }
    }

    render (){
        let { match } = this.props;
        return (
            <div>
                <Switch>

                    <Route path={`${match.url}/choice/:account_id`} component={AccountList} />
                    <Route path={`${match.url}/baskets/:account_id`} component={AccountInfo} />
                    <Route path={`${match.url}/addBasket/:account_id`} component={AddBasket} />
                    <Route path={`${match.url}/detail/:account_id`} component={AccountDetail} />
                    <Route path={`${match.url}/:account_id/:basket_id`} component={BasketInfo} />

                </Switch>
            </div>
        );
    }

}

Accounts.propTypes = {
    match: PropTypes.any
};


export default Accounts;
