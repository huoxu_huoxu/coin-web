/**
 * @description
 *  basket 添加篮子
 * 
 */

import React from "react";
import { Upload, message, Button, Icon } from 'antd';

import ModuleTitle from "../../modules/moduleHeader/moduleTitle";
import { uploadFile } from "../../requestApi";
import { tokenFail } from "../../libs/base";
import { loadingHide } from "../../modules/moduleLoading/moduleLoading";


class AddBasket extends React.Component {

    componentWillMount() {
        let action = `${window.api}${uploadFile}`;
        let { account_id } = this.props.match.params;

        this.attr = {
            name: 'backetsCsv',
            action,
            data: {
                account_id
            },
            beforeUpload (file){
                let names = file.name.split(".");
                let extname = names[names.length - 1];
                if (extname === "csv") return true;
                message.error("请上传 .csv 后缀的文件");
                return Promise.reject();                
            },
            onChange(info) {

                if (info.file.status !== 'uploading') {
                    console.log(" ... 已经完成 ");
                }

                if (info.file.status === 'done') {
                    message.success(`${info.file.name} 文件上传成功.`);
                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} 文件上传失败.`);
                    let { errcode } = info.file.response;
                    tokenFail(errcode, info.file.response);
                }

                // 上传失败的文件处理
                console.log(info.file.status);

            }
        };
    }

    // 解除由首屏渲染的loading状态 
    componentDidMount (){
        loadingHide();
    }

    render() {

        let dom = <Button size="large" onClick={window.history.back.bind(window.history)}>返回</Button>;

        return (
            <div>

                <ModuleTitle title="添加篮子" dom={dom} />

                <hr className="page_account_hr mar_b_30" />

                <Upload {...this.attr}>
                    <Button>
                        <Icon type="upload" /> 上传文件
                    </Button>
                </Upload>

            </div>
        );
    }

}


export default AddBasket;
