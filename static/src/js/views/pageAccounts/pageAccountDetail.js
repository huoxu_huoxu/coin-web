/**
 * @description
 *  account 账户详情 - 账户信息
 * 
 */

import React from "react";
import { Table, Row, Col, Button } from 'antd';
import PropTypes from "prop-types";
import { connect } from "react-redux";

import ModuleTitle from "../../modules/moduleHeader/moduleTitle";
import { DOWNLOAD_BALANCE } from "../../status/action"
import { accounts_container } from "../../status/status";
import { tab_sorter, dateConversion } from "../../libs/base";

class AccountDetail extends React.Component {
    state = {
        account_id: null,
        account_name: null,
        balance: [],
        sortedInfo: {},
        updated_at: null,
        btcTotal: 0,
        usdtTotal: 0,
    };

    componentWillMount (){
        let { get_data, nav_status } = this.props; 
        nav_status("accounts_detail");
        get_data();
    }

    componentWillReceiveProps (nextProps){
        let { match } = this.props; 
        let { accounts, userInfo } = nextProps;

        let { account_id } = match.params
        let { balance, updated_at } = accounts[account_id];

        let btcTotal = 0;
        let usdtTotal = 0;

        if (!balance){
            balance = [];
        } else {
            balance = JSON.parse(balance);
            for (let v of balance) {
                if (v["btc"] !== "N/A"){
                    btcTotal += v["btc"];
                    usdtTotal += v["usdt"];
                    v["btc"] = Number(v["btc"].toFixed(8));
                    v["usdt"] = Number(v["usdt"].toFixed(8));
                    v["balance"] = Number(v["balance"].toFixed(8));
                }
            }
        }

        this.setState(Object.assign({}, userInfo, { balance, updated_at, btcTotal, usdtTotal }));
    }

    handleChange (pagination, filters, sorter){

        // 排序
        let { sortedInfo } = this.state;

        // 区分触发函数的原因
        if (sorter.columnKey){
            if (sortedInfo.columnKey !== sorter.columnKey || sortedInfo.order !== sorter.order){
                let n = _.cloneDeep(this.state.balance);
                n = tab_sorter(n, sorter.columnKey, sorter.order);

                this.setState({
                    balance: n,
                    sortedInfo: sorter
                });
            }
            return ;
        }
        
        this.setState({
            sortedInfo: {}
        });
    }

    downloadPos (){
        // console.log(DOWNLOAD_BALANCE);
        DOWNLOAD_BALANCE({ account_id: this.state.account_id })
    }

    render (){
        let s = `${this.state.account_display_name || ""} 账户详情`;
        let { sortedInfo } = this.props;
        sortedInfo = sortedInfo || {};

        let columns = [
            {
                title: '币种',
                dataIndex: 'asset',
                key: 'asset',
                sorter: true,
                sortOrder: sortedInfo.columnKey === "asset" && sortedInfo.order
            }, {
                title: '余额',
                dataIndex: 'balance',
                key: 'balance',
                sorter: true,
                sortOrder: sortedInfo.columnKey === "balance" && sortedInfo.order
            },
            {
                title: '等值BTC',
                dataIndex: 'btc',
                key: 'btc',
                sorter: true,
                sortOrder: sortedInfo.columnKey === "btc" && sortedInfo.order
            },
            {
                title: '等值USDT',
                dataIndex: 'usdt',
                key: 'usdt',
                sorter: true,
                sortOrder: sortedInfo.columnKey === "usdt" && sortedInfo.order
            }
        ];

        let t = Date.now()
        let diffT = t - new Date(this.state.updated_at).getTime();
        let tableClassName = "page_sorted_table";
        if (diffT > 3600000 && diffT !== t) {
            tableClassName = "page_sorted_table page_sorted_table_background"
        } 

        return (
            <div>
                
                <ModuleTitle title={s} />
                <hr className="page_account_hr" />
                
                <Row style={{"margin-top": "10px", "lineHeight":"30px"}}>
                    <Col span={4}>
                        <h1>{`合计 USDT: ${this.state.usdtTotal.toFixed(4)}`}</h1>
                    </Col>
                    <Col span={4}>
                        <h1>{`合计 BTC: ${this.state.btcTotal.toFixed(4)}`}</h1>
                    </Col>
                    <Col span={2} offset={14}>
                        <Button onClick={this.downloadPos.bind(this)} icon="download">导出仓位</Button>
                    </Col>
                </Row>

                <section className="pa_tb_detail">
                    <Table
                        columns={columns}
                        dataSource={this.state.balance}
                        bordered
                        className={tableClassName}
                        pagination={false}
                        onChange={this.handleChange.bind(this)}
                        footer={() => `更新日期: ${dateConversion(this.state.updated_at)}`}
                    />

                </section>
                
            </div>
        );
    }

}

AccountDetail.propTypes = {
    get_data: PropTypes.func
};

const Container = connect(accounts_container.state, accounts_container.props)(AccountDetail);

export default Container;
