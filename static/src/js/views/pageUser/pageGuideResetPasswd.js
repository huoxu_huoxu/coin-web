/**
 * @description
 *  引导用户重置密码
 * 
 */

import React from "react";
import { message, Icon } from "antd";
import { getDom } from "../../libs/dom";
import { CHANGE_PASSWD } from "../../status/action";
import { loadingHide } from "../../modules/moduleLoading/moduleLoading";

import { router_really } from "../../libs/base";

/**
 * 密码强度检测 多正则检验
 * 
 */

const regPassword       = /^\w{6,18}$/;
const strengthes = [
    /[a-z]/,
    /[A-Z]/,
    /[0-9]/,
    /_/,
    /^\w{10,}$/
]

class GuideResetPasswd extends React.Component {

    state = {
        strength: "",
        b_pwd: "",
        b_spwd: ""
    };

    componentDidMount (){
        loadingHide();
    }

     // 增加回车监听
     listeningEnter (e, b){
        if (e.which === 13){
            this.submitData();
        }
    }

    async submitData (){
        
        let password = getDom(".page_gr_pd").value;
        let sure_password = getDom(".page_gr_spd").value;
        
        let tmp = {
            password,
            sure_password
        };

        // 检查用户输入
        for (let [ , v] of Object.entries(tmp)){
            if (!v){
                return message.error("有必填项,未填写");
            }
        }
        if (!regPassword.test(tmp["password"]) || !regPassword.test(tmp["sure_password"])){
            return message.error("输入格式不正确, 请输入最少6位最多18位 由数字、字母、下划线组成的密码");
        }
        if (password !== sure_password){
            return message.error("两次输入的密码不一致");
        }

        delete tmp["sure_password"];
        tmp["password"] = md5(tmp["password"]);

        if (tmp["password"] === "d47bd33854a15e8ec9e0819416db2507") return message.error("请更换密码");

        // req
        let { errcode } = await CHANGE_PASSWD(tmp);
        if (!errcode) {
            return this.props.history.push(`${router_really()}/main`);
        } 

        if (errcode === 1200 || errcode === 1100){
            this.props.history.push(`${router_really()}/`);
        }
    }

    __getPwdStrength (v){
        let i = 0;
        for (let reg of strengthes){
            if (reg.test(v)){
                i++;
            };
        }
        if (i === strengthes.length){
            return { strength: "3" };
        }
        if (i >= 3){
            return { strength: "2" };
        }
        return { strength: "1" };
    }

    verifyPassword (e){
        let v = e.target.value, tmp, sure_password;

        // 输入为空
        if (!v){
            return this.setState({
                strength: "",
                b_pwd: "",
                b_spwd: ""
            });
        }

        // 输入不符合规范
        if (!regPassword.test(v)) {
            return this.setState({ 
                strength: "",
                b_pwd: false
            });
        }
        tmp = {
            b_pwd: true
        };
        sure_password = getDom(".page_gr_spd").value;
        if (sure_password){
            if (sure_password === v){
                tmp["b_spwd"] = true;
            } else {
                tmp["b_spwd"] = false;
            }
        }
        let strength = this.__getPwdStrength(v);
        this.setState({
            ...strength,
            ...tmp
        });
    }

    verifySurePassword (e){
        let v = e.target.value, password;
        
        if (!v){
            return this.setState({
                b_spwd: ""
            });
        }

        password = getDom(".page_gr_pd").value;
        if (v !== password){
            this.setState({
                b_spwd: false
            });
        } else {
            this.setState({
                b_spwd: true
            });
        }
    }

    render (){
        return (
            <div className="page_gresetpwd_wrap">

                <div 
                    className="page_gtd_contain"
                    onKeyPress={this.listeningEnter.bind(this)}
                >

                    <h2>请修改初始密码</h2>

                    <section className="page_gtd_input">
                        <input
                            className="page_lnw_input page_gr_pd" 
                            type="password" 
                            placeholder="密码" 
                            onKeyUp={this.verifyPassword.bind(this)}     
                            maxLength="18"               
                        />
                        <aside className="page_gtd_icon">
                            {
                                do {
                                    if (this.state.b_pwd === true){
                                        <Icon type="check" className="input_success" />
                                    } else if (this.state.b_pwd === false) {
                                        <Icon type="close" className="input_fail" />
                                    }
                                }
                            }
                        </aside>
                    </section>

                    <section className="page_gtd_input">
                        <input 
                            className="page_lnw_input page_gr_spd" 
                            type="password" 
                            placeholder="确认密码"
                            onKeyUp={this.verifySurePassword.bind(this)}    
                            maxLength="18" 
                        />
                        <aside className="page_gtd_icon">
                            {
                                do {
                                    if (this.state.b_spwd === true){
                                        <Icon type="check" className="input_success" />
                                    } else if (this.state.b_spwd === false) {
                                        <Icon type="close" className="input_fail" />
                                    }
                                }
                            }
                        </aside>
                    </section>
                    
                    <seciton className={`page_gtd_strength strength${this.state.strength}`}>
                        <li>弱</li>
                        <li>中</li>
                        <li>强</li>
                    </seciton>

                    <div className="page_lnw_submit" onClick={this.submitData.bind(this)}>
                        <aside className="page_lnw_space">
                            <div className="page_lnws_main" >
                                <li>确认</li>
                                <i className="iconfont icon-right-circle-fill"></i>
                            </div>
                        </aside>
                    </div>

                </div>
              
            </div>
        );
    }

}

export default GuideResetPasswd;



