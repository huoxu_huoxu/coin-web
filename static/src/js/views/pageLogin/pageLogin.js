import React from "react";
import { message } from "antd";
import { getDom } from "../../libs/dom";
import { userLogin } from "../../requestApi";
import { setToken, requestFunc } from "../../libs/base";
import { loadingShow, loadingHide } from "../../modules/moduleLoading/moduleLoading";

import { REFRESH_VERIFY_IMAGE } from "../../status/action";
import { router_really } from "../../libs/base";


// ...
class Login extends React.Component {

    state = {
        // 记录image-token
        verifytImageToken: null
    }
    
    showVerifyImage (v, img){
        this.setState({
            verifytImageToken: v
        });
        setTimeout(() => {
            this.img.innerHTML = "";
            this.img.innerHTML = img;
        }, 0);
    } 

    async refreshVerifyImage (){
        let { errcode, data } = await REFRESH_VERIFY_IMAGE();
        if (!errcode){
            this.showVerifyImage(data.v, data.img);
        }
    }

    componentDidMount (){
        if (window.__server_render && window.__server_render.failure_times > 2){
            let { v, img } = window.__server_render;
            this.showVerifyImage(v, img);
            window.__server_render = null;
        }
    }

    // 增加回车监听
    listeningEnter (e){
        if (e.which === 13){
            this.submitData();
        }
    }

    gotoMain (pwd){
        // 原始密码密文
        if (pwd === "d47bd33854a15e8ec9e0819416db2507"){
            this.props.history.push(`${router_really()}/guideResetpwd`);
        } else {
            this.props.history.push(`${router_really()}/main`);
        }
    }

    async submitData (){
        
        let username = getDom(".page_login_username").value;
        let password = getDom(".page_login_password").value;
        let tmp = {
            email: username,
            password
        };

        // 是否需要验证码
        if (this.state.verifytImageToken){
            let verifyValue = +getDom(".page_login_verify").value;
            if (Number.isNaN(verifyValue)){
                return message.error("请填写验证图片的计算结果");
            }
            tmp["verifyValue"] = verifyValue;
            tmp["verifyToken"] = this.state.verifytImageToken;
        }

        // 检查用户是否填写必填项
        for (let [ , v] of Object.entries(tmp)){
            if (!v){
                message.error("有必填项,未填写");
                return ;
            }
        }


        tmp["password"] = md5(tmp["password"]);
        // req
        loadingShow();
        let { errcode, data } = await requestFunc(userLogin, "post", tmp);
        loadingHide();


        // succ
        if(!errcode){
            
            let { nickname, email, role } = data;
            setToken(nickname, email, role);
            
            return this.gotoMain(tmp["password"]);
        }

        // fail
        let { v, img, failure_times } = data;
        if ((failure_times && failure_times > 2) || errcode === 1006){
            return this.showVerifyImage(v, img);
        }
    }

    render (){
        return (
            <div className="page_login_new_wrap">

                <div 
                    className="page_lnw_contain"
                    onKeyPress={this.listeningEnter.bind(this)}
                >

                    <h1>QuantDog</h1>

                    <input 
                        className="page_lnw_input page_login_username" 
                        name="email" 
                        type="text" 
                        placeholder="邮箱/账号" 
                    />
                    <input 
                        className="page_lnw_input page_login_password" 
                        name="password" 
                        type="password" 
                        placeholder="密码" 
                    />

                    {
                        do {
                            if (this.state.verifytImageToken){
                                <section className="page_login_v_wrap">
                                    <div className="page_login_verifyInput">
                                        <input 
                                            name="text" 
                                            type="text" 
                                            className="page_login_verify"
                                            placeholder="验证码" 
                                        />
                                    </div>
                                    <div 
                                        className="page_login_verifyImage" 
                                        ref = {(img) => {this.img = img;}}
                                        title="点击刷新图片"
                                        onClick={this.refreshVerifyImage.bind(this)}
                                    >
                                    </div>
                                </section>
                            }
                        }
                    }

                    <div className="page_lnw_submit" onClick={this.submitData.bind(this)}>
                        <aside className="page_lnw_space">
                            <div className="page_lnws_main">
                                <li>登录</li>
                                <i className="iconfont icon-right-circle-fill"></i>
                            </div>
                        </aside>

                    </div>

                </div>

            </div>
        )
    }

}



export default Login;



