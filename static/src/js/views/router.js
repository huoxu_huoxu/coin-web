// router 
import React from "react";
import { 
    BrowserRouter as Router, 
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import { Provider } from "react-redux";
import PropTypes from "prop-types";
import STORE from "../status/store";
import { router_really } from "../libs/base";

// views-default
import VerifyUser from "./pageVerifyUser/pageVerifyUser";

// views-login
import Login from "./pageLogin/pageLogin";
import Accounts from "./pageAccounts/pageAccounts";
import PageUser from "./pageUser/pageUser";
import SuperAdmin from "./pageSuperAdmin/pageSuperAdmin";
import GuideResetPasswd from "./pageUser/pageGuideResetPasswd";

// modules
import ModuleLoading from "../modules/moduleLoading/moduleLoading";
import ModuleHeader from "../modules/moduleHeader/moduleHeader";
import ModuleFooter from "../modules/moduleFooter/moduleFooter";


// views-main
class MainRouter extends React.Component {

    componentDidMount (){

        // 将不好传递的全部在这里初始化
        if (!window.GLOBALS){
            window.GLOBALS = {};

            // 退出 - 用于 用户令牌过期, 强退
            window.GLOBALS.quit = () => {
                this.props.history.push(`${router_really()}/login`);
            };
        }
        
    }

    state = {
        routers: [
            {
                routeName: "accounts",
                com: Accounts,
                name: ""
            },
            {
                routeName: "user",
                com: PageUser,
                name: ""
            },
            {
                routeName: "superAdministrator",
                com: SuperAdmin,
                name: ""
            }
        ]
    }

    render (){
        let { match } = this.props;
        return (
            <div className="all_wrap">

                <ModuleHeader />

                <div className="page_main">
                    <section>
                        <Switch>
                            {
                                this.state.routers.map((item, index) => {
                                    return <Route key={index} path={`${match.url}/${item.routeName}`} component={item.com} />;
                                })
                            }
                            <Redirect to={`${match.url}/accounts`} />
                        </Switch>
                    </section>
                </div>

                <ModuleFooter />

            </div>
        );
    }
}

MainRouter.propTypes = {
    match: PropTypes.any
};

// 主内容模块, ssr需要
class Container extends React.Component {
    render (){
        return (
            <section>
                <Switch>
                    <Route exact path={`${router_really()}/`} component={VerifyUser} />
                    <Route path={`${router_really()}/login`} component={Login} />
                    <Route path={`${router_really()}/main`} component={MainRouter} /> 
                    <Route path={`${router_really()}/guideResetpwd`} component={GuideResetPasswd} />
                </Switch>
            </section>
        );
    }
}

// 一级主路由
const App = () => (
    <Provider store={STORE}>
        <Router>
            <div>
                <Container />
                <ModuleLoading />
            </div>
        </Router>
    </Provider>
);

export default App; 
export {
    Container
};
