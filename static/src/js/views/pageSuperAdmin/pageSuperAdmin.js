/**
 * @description
 *  superAdmin          超级管理员路由
 * 
 */


import React from "react";
import { connect } from "react-redux"; 
import { Tabs, Radio, Row, Col, DatePicker, Button, message } from "antd";

import { super_container } from "../../status/status";
import { GET_USERS, EXPORT_ORDERS, EXPORT_SETTLEMENT_PRICE } from "../../status/action";

const TabPane = Tabs.TabPane;
const RadioGroup = Radio.Group;
const { RangePicker } = DatePicker;


// 获取用户列表
const get_users = () => {

};


class PageSuperAdmin extends React.Component {

    state = {
        users: [],
        accounts: [],
        user_index: null,
        account_index: null,
        dates: [],
        iconLoading: false
    };

    async componentWillMount (){

        // 切换 nav
        let { nav_status } = this.props;
        nav_status("super");

        let { errcode, data } = await GET_USERS();
        // 直接停留
        if (errcode) return ;

        this.setState({
            users: data.list
        });
    }

    onChangeUserRadio ( { target } ){
        let index = target.value;
        this.setState({
            user_index: index,
            accounts: this.state.users[index]["accounts"],
            account_index: null
        });
    }

    onChangeAccountRadio ( { target } ){
        this.setState({
            account_index: target.value
        });
    }

    onChangeDates (dates){
        console.log(dates);
        console.log(dates[0] === dates[1])

        if (dates.length){
            let start = (new Date(dates[0])).toDateString();
            let end = (new Date(dates[1])).toDateString();
            if (start === end){
                return message.warn("起止时间不能选择同一天!")
            }
        }

        this.setState({
            dates
        });
    }

    exportCsv (){
        let tmp;
        if (!this.state.dates.length || this.state.user_index === null || this.state.account_index === null){
            return message.warn("有未填项, 请检查");
        }
        // 去时间部分, 保留日期部分, 转时间戳
        let started_at = (new Date(new Date(this.state.dates[0]).toDateString())).getTime();
        let ended_at = (new Date(new Date(this.state.dates[1]).toDateString())).getTime();

        let user = this.state.users[this.state.user_index];
        tmp = {
            user_id: user["id"],
            account_id: user["accounts"][this.state.account_index]["id"],
            started_at,
            ended_at
        };

        this.setState({
            iconLoading: true
        });

        return tmp;
    }

    exportOrdersCsv (){
        let tmp = this.exportCsv()
        if (!tmp) {
            return
        }
        EXPORT_ORDERS(tmp, () => {
            this.setState({
                iconLoading: false
            });
        });
    }

    exportSettlementPriceCsv (){
        let tmp = this.exportCsv()
        if (!tmp) {
            return
        }
        EXPORT_SETTLEMENT_PRICE(tmp, () => {
            this.setState({
                iconLoading: false
            });
        });
    }

    render (){

        // 成功拉取数据才能显示
        let page = "";
        if (this.state.users.length > 0){
            page = <Tabs defaultActiveKey="1" animated={false} >
                {/* 导出订单 */}
                <TabPane tab="导出订单" key="1">
                    <Row className="page_super_row">
                        <Col span="3">
                            选择用户
                        </Col>
                        <Col>
                            <RadioGroup onChange={this.onChangeUserRadio.bind(this)} value={this.state.user_index}>
                                {
                                    this.state.users.map((v, i) => {
                                        return <Radio 
                                            key={i} 
                                            value={i}
                                        >
                                            {v.nickname}
                                        </Radio>
                                    })
                                }
                            </RadioGroup>
                        </Col>
                    </Row>

                    {
                        do {
                            if (this.state.accounts.length){
                                <Row className="page_super_row">
                                    <Col span="3">
                                        选择账户
                                    </Col>
                                    <Col>
                                        <RadioGroup onChange={this.onChangeAccountRadio.bind(this)} value={this.state.account_index}>
                                            {
                                                this.state.accounts.map((v, i) => {
                                                    return <Radio 
                                                        key={i} 
                                                        value={i}
                                                    >
                                                        {v.display_name}
                                                    </Radio>
                                                })
                                            }
                                        </RadioGroup>
                                    </Col>
                                </Row>
                            }
                        }
                    }
                    
                    <Row className="page_super_row">
                        <Col span="3">
                                起止时间
                        </Col>
                        <Col>
                            <RangePicker 
                                onChange={this.onChangeDates.bind(this)} 
                                value={this.state.dates}
                            />
                        </Col>
                    </Row>

                    <Row className="page_super_row">
                        <Button loading={this.state.iconLoading} onClick={this.exportOrdersCsv.bind(this)}>
                            导出订单
                        </Button>
                    </Row>

                </TabPane>

                {/* 导出结算价 */}
                <TabPane tab="导出结算价" key="2">
                    
                    <Row className="page_super_row">
                        <Col span="3">
                            选择用户
                        </Col>
                        <Col>
                            <RadioGroup onChange={this.onChangeUserRadio.bind(this)} value={this.state.user_index}>
                                {
                                    this.state.users.map((v, i) => {
                                        return <Radio 
                                            key={i} 
                                            value={i}
                                        >
                                            {v.nickname}
                                        </Radio>
                                    })
                                }
                            </RadioGroup>
                        </Col>
                    </Row>

                    {
                        do {
                            if (this.state.accounts.length){
                                <Row className="page_super_row">
                                    <Col span="3">
                                        选择账户
                                    </Col>
                                    <Col>
                                        <RadioGroup onChange={this.onChangeAccountRadio.bind(this)} value={this.state.account_index}>
                                            {
                                                this.state.accounts.map((v, i) => {
                                                    return <Radio 
                                                        key={i} 
                                                        value={i}
                                                    >
                                                        {v.display_name}
                                                    </Radio>
                                                })
                                            }
                                        </RadioGroup>
                                    </Col>
                                </Row>
                            }
                        }
                    }
                    
                    <Row className="page_super_row">
                        <Col span="3">
                                起止时间
                        </Col>
                        <Col>
                            <RangePicker 
                                onChange={this.onChangeDates.bind(this)} 
                                value={this.state.dates}
                            />
                        </Col>
                    </Row>

                    <Row className="page_super_row">
                        <Button loading={this.state.iconLoading} onClick={this.exportSettlementPriceCsv.bind(this)}>
                            导出结算价
                        </Button>
                    </Row>

                </TabPane>
            </Tabs>;
        }

        return (
            <div>
                {page}
            </div>
        );
    }

}

const container = connect(super_container.state, super_container.props)(PageSuperAdmin);

export default container;
