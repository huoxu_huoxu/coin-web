/**
 * @description
 *  所有需要用到的api
 * 
 */

const testToken =               "api/verify/token";

const userLogin =               "webapi/user";
const changePasswd =            "webapi/user";
const autoLogin =               "webapi/user";
const verifyImage =             "webapi/user/verifyImage";


const superGetUsers =            "webapi/users";

const getAccounts =             "api/data/getAccounts";
const getBaskets =              "api/data/getBaskers/:";
const uploadFile =              "api/baskets/upload";
const getOrders =               "api/data/getOrders/:/:";
const downloadBasket =          "api/download/:account_id/:basket_id";
const updateOrder =             "api/order/:account_id/:basket_id/:order_id";

const basketApi =               "api/basket/:account_id/:basket_id";

const superExportOrders =       "api/data/orders";
const superExportSettlementPrice = "api/data/walletRecorded";

const downloadAccountBalance =  "api/download/accountBalance/:account_id";

const saveToken =               "webapi/saveToken";
const destroyToken =            "webapi/destroyToken";



export {
    testToken,
    userLogin,
    getAccounts,
    getBaskets,
    uploadFile,
    getOrders,
    saveToken,
    downloadBasket,
    updateOrder,
    basketApi,
    destroyToken,
    superGetUsers,
    superExportOrders,
    superExportSettlementPrice,
    changePasswd,
    autoLogin,
    verifyImage,
    downloadAccountBalance
};

