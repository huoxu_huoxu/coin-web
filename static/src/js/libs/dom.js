// 操作dom方法封装


const getDom = (sClass) => {
    return document.querySelector(sClass);
};

// 获取当前页面上所有 input 的键值对
const getInputs = (str = "input") => {
    let obj = {};
    let aInput = document.querySelectorAll(str);
    for(let item of aInput){
        if(item.name){
            obj[item.name] = item.value;
        }
    }
    return obj;
};

const setInputs = (str = "input", dict) => {
    let aInput = document.querySelectorAll(str);
    for (let input of aInput){
        let key = input.getAttribute("curr") || input.name;
        input.value = dict[key] || "";
    }
};

const clearInputs = (str = "input") => {
    let aInput = document.querySelectorAll(str);
    for(let item of aInput){
        item.value = "";
    }
};

export {
    getDom,
    getInputs,
    clearInputs,
    setInputs
};


