/**
 * @description
 *  web worker
 * 
 * @desc
 * 
 *  init                        建立worker初始化
 * 
 * 
 * 
 */

 // master 前置动作
const actions = {
    init: { name: "init" }
}

// 反馈动作处理器
const faction_handler = (action) => {
    switch (action.name){
        case "finit": {
            return console.log("worker 建立完成");
        }
        default: {
            console.log("master 内 无匹配 ...");
        }
    }
};


// webworker
class DiyWebWorker {

    constructor (){
        this.__worker = null;
    }

    start (){
        if (!this.__worker){
            this.__worker = new Worker(window.location.origin + "/dist/worker.js")
            this.__worker.onmessage = ({ data }) => {
                faction_handler(data);
            }
            // init
            this.__worker.postMessage(actions.init);
        }
    }

    send (name, data){
        worker.postMessage();
    }

}

const diyWebWorker = new DiyWebWorker();

export default diyWebWorker;
