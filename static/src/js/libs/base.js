// base
import { message } from 'antd';

const private_attr = new Map();
private_attr.set("__nickname", Symbol("nickname"));
private_attr.set("__username", Symbol("username"));
private_attr.set("__token", Symbol("token"));
private_attr.set("__role", Symbol("role"));
private_attr.set("__default_account_id", Symbol("default_account_id"));


// 用户信息暂存
class User {
    constructor (){
        this.init();
    }

    set (nickname, username, role, default_account_id = ""){
        this[private_attr.get("__nickname")] = nickname; 
        this[private_attr.get("__username")] = username;
        this[private_attr.get("__role")] = role;
        this[private_attr.get("__default_account_id")] = default_account_id;
    }

    get role (){
        return this[private_attr.get("__role")];
    }

    get nickname (){
        return this[private_attr.get("__nickname")];
    }

    get account_id (){
        return this[private_attr.get("__default_account_id")]; 
    }

    set account_id (account_id){
        this[private_attr.get("__default_account_id")] = account_id; 
    }

    init (){
        this[private_attr.get("__nickname")] = null; 
        this[private_attr.get("__username")] = null;
    }

    clear (){
        this.init();
    }

    set_localStorage (key, v){
        localStorage.setItem(key, v);
    }
}

const user = new User();


// 特殊错误
const requestErr = {
    errcode: 99999,
    msg: "网络通信失败"
};

// 登录过期, 返回初始
const tokenFail = (errcode, { msg }) => {

     // The user logged in, token problem
    if (/^(1100|1102|5400|1200|1202)$/.test(errcode)){
        user.clear();
        message.error(msg);
        window.GLOBALS && window.GLOBALS.quit();
        return ;
    }

    if (errcode !== 0){
        message.error(msg);
    }   
};

// 发起请求封装
const requestFunc = async (url, method, data = {}) => {
    method = method.toLowerCase();
    let sUrl = `${window.api}${url}`, s = "";
    if(method === "get" || method === "delete"){
        for(let [key, name] of Object.entries(data)){
            if(!s){
                s += `?${key}=${name}`;
                continue;
            }
            s += `&${key}=${name}`;
        }
        sUrl += s;
    }

    try{
        let resul = await axios[method](sUrl, data);
        if(!resul.data){
            return requestErr;
        }
        // 登录过期
        tokenFail(resul.data.errcode, resul.data);
        return resul.data;
    } catch(err) {
        console.log(err.status);
        message.error(err.toString());
    }
    return requestErr;
};

// 创建可用的URL
const create_url = (url) => {
    return `${window.api}${url}`;
};


// 设置/获取 token
const setToken = (nickname, username, role) => {
    user.set(nickname, username, role, window.localStorage.getItem("default_account_id"));
    window.localStorage.setItem("nickname", nickname);
    window.localStorage.setItem("username", username);
    window.localStorage.setItem("role", role);
};

const getToken = () => {
    let default_account_id = window.localStorage.getItem("default_account_id") || null;
    let nickname = window.localStorage.getItem("nickname") || null;
    let username = window.localStorage.getItem("username") || null;
    let role = window.localStorage.getItem("role") || null;
    user.set(nickname, username, role, default_account_id);
};


// 转换时间
const dateConversion = function (str){
    let oDate = new Date(str);
    let y = oDate.getFullYear();
    let m = oDate.getMonth() + 1;
    let d = oDate.getDate();
    let h = oDate.getHours();
    let mm = oDate.getMinutes();
    let s = oDate.getSeconds();

    // y = (y+"").substr(2);
    m = m < 10 ? '0' + m : m;
    d = d < 10 ? '0' + d : d;
    h = h < 10 ? '0' + h : h;
    mm = mm < 10 ? '0' + mm : mm;
    s = s < 10 ? '0' + s : s;

    return `${y}/${m}/${d} ${h}:${mm}:${s}`;
};


// : 类型url生成
const urlSynthesis = (url, ...argv) => {
    let s = argv.join("/");
    let a_url = url.split(":");
    return [ a_url[0], s ].join("");
};


// 类型检测
const test = (() => {

    const type_arr = [
        "Function",
        "Object",
        "Array",
        "Symbol",
        "Set",
        "Map",
        "WeakSet",
        "WeakMap",
        "String",
        "Number",
        "Boolean"
    ];

    const tmp = {};

    for ( const type_name of type_arr){
        tmp[`is${type_name}`] = (v) => {
            return Object.prototype.toString.call(v) === `[object ${type_name}]`;
        };
    }

    // isNumber 方法 - 追加条件
    const tmp_number = tmp["isNumber"];
    tmp["isNumber"] = (v) => tmp_number(v) && !Number.isNaN(v);

    return tmp;

})();


// 统一排序方案
const tab_sorter = (list, key, direction) => {
    
    let n = (() => {
        let n = _.cloneDeep(list);

        // number比较
        if (test.isNumber(+n[0][key]) || n[0][key] === "N/A"){
            n.sort( (a, b) => {
                let va = +a[key] || 0;
                let vb = +b[key] || 0;

                if (direction === "ascend") return va - vb;
                else return vb - va;
            } );
            return n;
        } 

        // 日期排序
        if (key === "started_at" || key === "ended_at"){
            n.sort( (a, b) => {
                if (direction === "ascend") return new Date(a[key]) < new Date(b[key]);
                else return new Date(a[key]) > new Date(b[key]);
            });
            return n;
        }

        // 判断需要比对的字符串是否全部相同
        let s = n[0][key], j=1;
        for (let i=1; i<n.length; i++){
            let v = n[i];
            if (v[key] === s){
                j++;
                continue;
            }
            break;
        }
        if (j === n.length) return n;

        // 非全部相同 - 字符串排序
        n.sort( (a, b) => {
            let c = 0;
            while (true){
                let va = a[key].codePointAt(c);
                let vb = b[key].codePointAt(c);

                if (va === undefined) return -1;
                if (vb === undefined) return 1;

                if (va > vb) return -1;
                if (va < vb) return 1;

                c++;
            }
        } );

        // 完全一样 ... 会有 bug
        if (direction === "ascend"){
            n.reverse();
        }
        return n;

    })();

    return n;
};

// 手动转换科学计数法
const toE = (num) => {
    // 求以10为低的num的自然对数, 并且向下取整
    let logarithm = Math.floor(Math.log(num) / Math.LN10);
    // 将自然对数取反, 正数/负数 转有效整数位为1的数
    num = (num * Math.pow(10, -logarithm)).toFixed(3);
    return num + "e" + (logarithm > 0 ? "+" + logarithm : logarithm);
};


// 路由兼容方案 解决 开发 / 测试 环境的差异
// 继承型 通过this.props.match.url, 平级切换型 通过此函数拿到代理级路由
const router_really = () => {
    try{
        if (/localhost/.test(window.location.href)) return "";
    } catch (err){
        // pass
    }
    return "/web";
};



export {
    setToken,
    getToken,
    requestFunc,
    user,
    dateConversion,
    tokenFail,
    urlSynthesis,
    create_url,
    test,
    tab_sorter,
    router_really,
    toE
};
