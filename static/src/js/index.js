// 样式打包
import "../stylesheets/main.min.css";

// 入口
import React from 'react';
import ReactDOM from 'react-dom';

import App from "./views/router";
import { getToken } from "./libs/base";


// 初始化, 分离环境
{
	if(/localhost/.test(location.href)){
		window.api = "http://localhost:3001/";
	} else {
		window.api = window.location.origin + "/web/";
	}

	// debug调试模式
	if (window.location.search.includes("test=1")){
		window.debug = "development";
	}
	if (~window.location.href.indexOf(3001)){
		window.debug = "development";
	}

	// 读取 localStorage
	getToken();
}

// min-height 兼容
{
	(() => {
		let iH = window.screen.availHeight - 70 - 120 - 20 - 10;
		let oStyle = document.createElement("style");
		let oHeader = document.getElementsByTagName("head")[0];
		oStyle.innerText = `.page_main{min-height:${iH}px;padding:20px;}`;
		oHeader.appendChild(oStyle);
	})();
}

// 挂载dom
{
	ReactDOM.render(
		<App />, 
		document.getElementById('app')
	);
}

