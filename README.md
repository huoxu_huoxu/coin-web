#### coin web

##### 启动

	npm start		启动纯前端研发环境(前端独立, ssr后不能单启动了)
	npm run watch_dev	启动与sv_dev配套的前端监控与编译研发环境
	npm run build		启动生产环境打包
	npm run sass		启动前端研发scss监听
	npm run sv_dev		启动研发环境中间层服务器, 支持ssr
	npm run sv_pro		启动生产环境中间层服务器


